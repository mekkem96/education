jQuery(document).ready(function($) {
    $(document).ready(function() {

        /* Set Width of Header Elements when fixed */
        function adjustDesktopWidth() {
            var parentwidth = $('#header-desktop').width();
            $('#header-desktop #header-content').width(parentwidth);
            $(".mega-nav").width(parentwidth);
        }

        function adjustMobileWidth() {
            var parentwidth = $('#header-mobile').width();
            $('#header-mobile #header-content-mobile').width(parentwidth);
        }

        adjustDesktopWidth();
        adjustMobileWidth();

        $(window).resize(function() {
            adjustDesktopWidth();
            adjustMobileWidth();
        });


        // MAIN NAV DROP DOWN
        $('#main-menu').hoverIntent({
            over: slideDown,
            out: slideUp,
            selector: 'li',
            timeout: 500
        });

        function slideDown(){
            $(this).find("ul:first").stop().fadeToggle('fast');
        }
        function slideUp(){
            $(this).find("ul:first").stop().fadeToggle('fast');
        }

        // HAMBURGER NAV
        $('.burger-nav').click(function() {
            $('.fly-out-wrapper').stop().animate({left: '0'});
        });

        // CLOSE MOBILE NAV
        $('.top-area button').click(function() {
            $('.fly-out-wrapper').stop().animate({left: '-100%'});
        });


        //$("#main-menu").slideMobileMenu();


        // LATEST RECIPES SLIDER
        $('.recipe-slider').slick({
            autoplay: false,
            autoplaySpeed: 5000,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            speed: 1000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 680,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });


        // CUISINES SLIDER
        $('.cuisines-slider').slick({
            autoplay: false,
            autoplaySpeed: 5000,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            speed: 1000,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 680,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });


        // DIETARY SLIDER
        $('.dietary-slider').slick({
            autoplay: false,
            autoplaySpeed: 5000,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            speed: 1000,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 680,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        // COLLECTIONS SLIDER
        $('.collection-slider').slick({
            autoplay: false,
            autoplaySpeed: 5000,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            speed: 1000,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 680,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        // CATEGORY SLIDER
        $('.category-slider').slick({
            autoplay: false,
            autoplaySpeed: 5000,
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 4,
            speed: 1000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 680,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });


        $('.layout-options').on('click','span', function(){
            $(this).addClass('active').siblings().removeClass('active');
        });


        // LAYOUT CHANGE FOR RECIPES
        $('.layout-options .double').click(function() {
            $('.recipe-list').addClass('double');
            $('.recipe-list').removeClass('single');
            $('.article-list').addClass('double');
            $('.article-list').removeClass('single');
        });

        $('.layout-options .single').click(function() {
            $('.recipe-list').addClass('single');
            $('.recipe-list').removeClass('double');
            $('.article-list').addClass('single');
            $('.article-list').removeClass('double');
        });


        // COOK'S DICTIONARY TABS
        $(function() {
            $('#dictionary-tabs').tabs({
                collapsible: true
            });
        });
    });
});