$(document).ready(function() {

	checkMobile();

	var docHeight = $(document).outerHeight(),
		gutterHeight = 20,
		recipesHidden = false;

	//Set margin top on main content based on menu height
	setInterval(doSomething, 1);

	function doSomething() {
		if ($(window).width() <= 1024) {
	        $('.main-content.content').css({'margin-top': $('.visible-mobile.sticky-header').innerHeight()});
	    } else {
	    	$('.main-content.content').css({'margin-top': $('.sticky-header').innerHeight()});
	        $('#header-desktop').css({'height': $('.header-wrapper').outerHeight()});
	    }

    };

	if ($(window).width() <= 375) {
		headerBottom = $('#header-mobile').position().top + $('#header-desktop').outerHeight();
	} else {
		headerBottom = $('nav').position().top + $('nav').outerHeight();
	}


	$('.anchors a').click(function() {
		var scrollTo = $(this).data('anchor');
		$('html').animate({
			scrollTop: $('#'+scrollTo).offset().top - ( ($('.header-wrapper').outerHeight()) + ($('.nav-page').outerHeight() + gutterHeight) )
		}, 300);
	});

	$(window).scroll(function() {
		checkScroll();
	});


	function checkScroll() {

		if ($(window).width() <= 375) {
			headerBottom = $('#header-mobile').position().top + $('#header-mobile2').outerHeight();
		} else {
			headerBottom = $('header.sticky-header').position().top + $('header.sticky-header').outerHeight();
		}

		if ($(window).scrollTop() > 200) {
			$('body').addClass('scrolled');
		} else {
			$('body').removeClass('scrolled');
		}

		if ($(window).scrollTop() + $(window).outerHeight() / 3 > docHeight / 3) {
			$('body').addClass('scrolled-half');
		} else {
			$('body').removeClass('scrolled-half');
		}

		if ($(window).scrollTop() + $(window).outerHeight() + 1 > docHeight && !recipesHidden) {
			$('.related-recipes').addClass('visible');
		}

		//Start of sidebar sticky - Set ad container heights

		$('.single-recipe .side-top-ad').css({'height': $('.recipe-ingredients').outerHeight()});
	    $('.single-recipe .side-bot-ad').css({'height': $('.rate-review').outerHeight()});

		var singleSidebar = $('.standard-content ').outerHeight() / 2,
	    singleRelated = $('.related-articles ').outerHeight() / 2;
        if ( $( ".related-articles" ).length ) {
        	$('.single .side-top-ad, .single .side-bot-ad').css({'height': singleSidebar - singleRelated});
        } else {
        	$('.single .side-top-ad, .single .side-bot-ad').css({'height': singleSidebar});
        }

		$('.single-recipe .sidebar.ad, .single .sidebar.ad').each(function() {
			var sidebar = $(this),
			sidebarTop = sidebar.offset().top - $(window).scrollTop(),
			sidebarBottom = sidebarTop + sidebar.outerHeight(),
			section = $(this).parent(),
			sectionTop = section.offset().top - $(window).scrollTop(),
			//sectionRight = section.offset().left + section.outerWidth(),
			sectionBottom = sectionTop + section.outerHeight();

			if (sidebarTop < sectionTop) {
				sidebar.removeClass('fixed')
				.removeAttr('style')
				.removeClass('bottom');
			}//stop when reach top

			if (sidebarTop < headerBottom && !sidebar.hasClass('bottom')) {
				sidebar.addClass('fixed')
				.css('width', section.outerWidth())
				.css('top', headerBottom);
			}//Top to bottom

			if (sidebarBottom > sectionBottom) {
				sidebar.removeClass('fixed')
				.addClass('bottom')
				.removeAttr('style')
				.css('width', section.outerWidth());
			}// Stop when at bottom
			if (sidebar.hasClass('bottom') && sidebarTop > headerBottom) {
				sidebar.addClass('fixed')
				.css('width', section.outerWidth())
				.css('top', headerBottom)
				.removeClass('bottom');
			} //Bottom to Top

			if (sidebar.hasClass('fixed')) {
				sidebar.css('top', headerBottom);
			} //Not sure...

		});


		$('.home .sidebar, .archive .sidebar, .category .sidebar').each(function() {
			var sidebar = $(this),
			sidebarTop = sidebar.offset().top - $(window).scrollTop(),
			sidebarBottom = sidebarTop + sidebar.outerHeight(),
			section = $(this).parent(),
			sectionTop = section.offset().top - $(window).scrollTop(),
			//sectionRight = section.offset().left + section.outerWidth(),
			sectionBottom = sectionTop + section.outerHeight();

			if (sidebarTop < sectionTop) {
				sidebar.removeClass('fixed')
				.removeAttr('style')
				.removeClass('bottom');
			}

			if (sidebarTop < headerBottom && !sidebar.hasClass('bottom')) {
				sidebar.addClass('fixed')
				.css('width', section.outerWidth())
				.css('top', headerBottom);
			}

			if (sidebarBottom > sectionBottom) {
				sidebar.removeClass('fixed')
				.addClass('bottom')
				.removeAttr('style')
				.css('width', section.outerWidth());
			}
			if (sidebar.hasClass('bottom') && sidebarTop > headerBottom) {
				sidebar.addClass('fixed')
				.css('width', section.outerWidth())
				.css('top', headerBottom)
				.removeClass('bottom');
			}

			if (sidebar.hasClass('fixed')) {
				sidebar.css('top', headerBottom);
			}
		});
		//Sidebar end


		//Top Ad Sticky Start
		//$('.banner-ad').each(function() {
		//	if ($(window).outerWidth() > 1024) {
		//		var $heightTest = $('.top-banner-ad-desktop').innerHeight();
		//		var bannerTop = $(this).offset().top - $(window).scrollTop();
		//		if (bannerTop < $(window).outerHeight()) {
		//			$('.hide-mobile.sticky-header').css({"transform":"translateY(-" + $heightTest + "px)"});
		//			$('.main-content.content').css({'margin-top': $('.sticky-header').offsetHeight});
		//		} else {
		//			$('.hide-mobile.sticky-header').removeAttr('style');
		//			$('.main-content.content').css({'margin-top': $('.sticky-header').offsetHeight});
		//		}
		//	} else {
		//		var $heightTestMob = $('.top-banner-ad-mobile').outerHeight();
		//		var bannerTop = $(this).offset().top - $(window).scrollTop();
		//		if (bannerTop < $(window).outerHeight()) {
		//			$('.sticky-header').css({"transform":"translateY(-" + $heightTestMob + "px)"});
		//			$('.main-content.content').css({'margin-top': $('.sticky-header').offsetHeight});
		//		} else {
		//			$('.sticky-header').removeAttr('style');
		//			$('.main-content.content').css({'margin-top': $('.sticky-header').offsetHeight});
		//		}
		//	}
		//});

		//Hide top ad when page is scrolled half
		if ($(window).outerWidth() > 1024) {
			var $heightTest = $('.top-banner-ad-desktop').innerHeight();
			if ( $( "body" ).is( ".scrolled" ) ) {
				$('.top-banner-ad-desktop').css({"height":"0px","overflow":"hidden","opacity":"0","margin-top":"0"});
			    $('.sticky-header').css({"transform":"translateY(-" + $heightTest + "px)"});
			    $('.main-content.content').css({'margin-top': $('.sticky-header').offsetHeight});
			    $('.sticky-header').addClass('fixed');
			}
			else{
				$('.sticky-header').removeAttr('style');
				$('.top-banner-ad-desktop').removeAttr('style');
				$('.main-content.content').css({'margin-top': $('.sticky-header').offsetHeight});
				$('.sticky-header').removeClass('fixed');
			}
		} else {
			var $heightTestMob = $('.top-banner-ad-mobile').outerHeight();
			if ( $( "body" ).is( ".scrolled" ) ) {
				$('.sticky-header').css({"transform":"translateY(-" + $heightTestMob + "px)"});
				$('.main-content.content').css({'margin-top': $('.sticky-header').offsetHeight});
				$('.sticky-header').addClass('fixed');
			} else {
				$('.sticky-header').removeAttr('style');
				$('.main-content.content').css({'margin-top': $('.sticky-header').offsetHeight});
				$('.sticky-header').removeClass('fixed');
			}
		}

		$('.mobile-banner-ad').each(function() {
			var banner = $(this),
			bannerTop = $(this).offset().top,
			firstBanner = $('.top-banner-ad'),
			firstBannerTop = firstBanner.offset().top;

			if (bannerTop <= firstBannerTop) {
				banner.addClass('fixed').css('z-index', 999);
			}
		});

		if ($('.sticky-header').length) {

			// Desktop

			//if ($('.sticky-header').offset().top - $(window).scrollTop() >= 200) {
			//	$('.sticky-header').addClass('fixed');
			//	$('.sticky-header').addClass('fixed');
			//}

			// Find height of advert container ( plus 65px (10 top, 40 bottom) for CSS margin ) to use as 'scrollTop' value below
			//var advertHeightDesktop = (($(".top-banner-ad-desktop").height()) + 200);
			
			//if ($(window).scrollTop() <= advertHeightDesktop) {
			//	$('.sticky-header').removeClass('fixed');
			//}

			// Mobile

			//if ($('.sticky-header').offset().top - $(window).scrollTop() <= 200) {
			//	$('.sticky-header').addClass('fixed');
			//}

			// Find height of advert container ( plus 10px (0 top, 10 bottom) for CSS margin ) to use as 'scrollTop' value below
			//var advertHeightMobile = (($(".top-banner-ad-mobile").height()) + 200);

			//if ($(window).scrollTop() <= advertHeightMobile) {
			//	$('.sticky-header').removeClass('fixed');
			//}

			// Sticky page anchor navigation bar
			if (headerBottom + $(window).scrollTop() >= $('.recipe-directions').offset().top) {
				$('header').addClass('recipe-view');
			}
			if (headerBottom + $(window).scrollTop() <= $('.recipe-directions').offset().top) {
				$('header').removeClass('recipe-view');
			}

			if ($('#ingredients').offset().top <= headerBottom + 100 + $(window).scrollTop() && headerBottom + $(window).scrollTop() <= $('#ingredients').offset().top + $('#ingredients').outerHeight()) {
				//$('.anchors div:not(.ingredients) [data-fa-i2svg]').attr('data-prefix', 'far');
				//$('.anchors .ingredients [data-fa-i2svg]').attr('data-prefix', 'fas');
				//$('.anchors .ingredients [data-fa-i2svg]').attr('data-prefix', 'fas');
				$('.anchors a').removeClass('anchor-active');
				$('.anchors .ingredients a').addClass('anchor-active');
			}
			if ($('#method').offset().top <= headerBottom + 200 + $(window).scrollTop() && headerBottom + $(window).scrollTop() <= $('#method').offset().top + $('#method').outerHeight()) {
				//$('.anchors div:not(.method) [data-fa-i2svg]').attr('data-prefix', 'far');
				//$('.anchors .method [data-fa-i2svg]').attr('data-prefix', 'fas');
				$('.anchors a').removeClass('anchor-active');
				$('.anchors .method a').addClass('anchor-active');
			}
			if ($('#recipe-tips').offset().top <= headerBottom + 100 + $(window).scrollTop() && headerBottom + $(window).scrollTop() <= $('#recipe-tips').offset().top + $('#recipe-tips').outerHeight()) {
				//$('.anchors div:not(.method) [data-fa-i2svg]').attr('data-prefix', 'far');
				//$('.anchors .method [data-fa-i2svg]').attr('data-prefix', 'fas');
				$('.anchors a').removeClass('anchor-active');
				$('.anchors .tips a').addClass('anchor-active');
			}
		}
	}

	$(window).resize(function() {
		checkMobile();
	});

	function checkMobile() {

		if ($(window).width() <= 375) {
			$('.top-banner-ad:not(.non-stick)').prependTo('body');
		} else {
			$('.top-banner-ad:not(.non-stick)').insertBefore('.main-nav');
		}

	}

	$('.hide-recipes-bar').click(function() {
		recipesHidden = true;
		$('.related-recipes').removeClass('visible');
	})

});