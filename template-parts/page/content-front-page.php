<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>

    <h1><?php the_title(); ?></h1>
    <?php the_content(); ?>
