    <div class="recipe-stats nav-list">
        <ul>
            <li class="recipe-rating">
                <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
            </li>
            <li class="recipe-skill">
                <span class="icon-inline icon-effort-main">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                </span>
                <?php if(get_field('difficulty') == 'Easy'): ?>
                    <span>Easy</span>
                <?php elseif (get_field('difficulty') == 'Medium'): ?>
                    <span>Medium</span>
                <?php elseif (get_field('difficulty') == 'Challenge'): ?>
                    <span>A challenge</span>
                <?php endif; ?>
            </li>
        </ul>
    </div>