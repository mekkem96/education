<?php
/**
 * Template part for displaying page layouts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>

    <?php
    // Check if the flexible content field has rows of data
    if(have_rows('layout_options')):
        // Loop through the rows of data
        while (have_rows('layout_options')) : the_row();
            // STANDARD WIDE OR NARROW CONTENT
            if(get_row_layout() == 'standard_content'):
                $content_width = get_sub_field('select_content_width');
                $content = get_sub_field('content');

                if($content_width == 'Narrow'):
                    $content_width_class = 'narrow';
                else:
                    $content_width_class = '';
                endif;
                ?>
	                <div class="standard-content <?php echo $content_width_class; ?>">
                        <?php echo $content; ?>
                    </div>
                <?php
            endif;
            // VIDEO EMBED
            if(get_row_layout() == 'video_embed'):
                $video_embed = get_sub_field('video_url');
                $video_caption = get_sub_field('video_caption');
                ?>
                    <div class="video-container">
                        <div class="video-wrapper">
                            <?php echo $video_embed; ?>
                        </div>

                        <p><?php echo $video_caption; ?></p>
                    </div>
                <?php
            endif;
            // BLOCKQUOTE
            if(get_row_layout() == 'blockquote'):
                $quote_text = get_sub_field('quote_text');
                ?>
                    <div class="blockquote">
                        <p><?php echo $quote_text; ?></p>
                    </div>
                <?php
            endif;
            // METHOD OR STEP BY STEP
            if(get_row_layout() == 'method_or_step_by_step'):
                $instruction_text = get_sub_field('instructions');
                ?>
                    <div class="recipe-method text-standard content-block double-margin col-gutter">
                        <?php echo $instruction_text; ?>
                    </div>
                <?php
            endif;
        endwhile;
    else:
        // no layouts found
        echo '';
    endif;
    ?>