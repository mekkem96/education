<?php
/**
 * Template part for displaying page subscription content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

    <section id="subscribe-section">
        <div class="row no-gutters">
            <div class="col-sm-12">
                <div class="newsletter-wrapper content-block double-margin">
                    <h2>Sign up to our newsletter</h2>
                    <p>Sign up for our newsletter to stay up to date with all the latest news, recipes and offers.</p>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="magazine-wrapper">
                    <h2>Subscribe to our magazine</h2>

                    <?php if (get_field('postal_magazine_text', 'options')): ?>
                        <p><?php the_field('postal_magazine_text', 'options'); ?></p>
                    <?php endif; ?>

                    <?php if (get_field('postal_magazine_url', 'options')): ?>
                        <a href="<?php the_field('postal_magazine_url', 'options'); ?>" target="_blank" title="Subscribe to Delicious magazine" class="bttn">Subscribe</a>
                    <?php endif; ?>

                    <?php
                    $postal_magazine_image = get_field('postal_magazine_image', 'options');

                    if(!empty($postal_magazine_image)):
                    ?>
                        <div class="postal-magazine-cover">
                            <img src="<?php echo $postal_magazine_image['url']; ?>" alt="<?php echo $postal_magazine_image['alt']; ?>" class="img-responsive" />
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="digital-download-wrapper">
                    <h2>Download our digital version</h2>

                    <?php if (get_field('digital_magazine_text', 'options')): ?>
                        <p><?php the_field('digital_magazine_text', 'options'); ?></p>
                    <?php endif; ?>

                    <div class="app-badges">
                        <a href="https://itunes.apple.com/gb/app/delicious.-magazine-uk/id577791504?mt=10" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/app-store-badge.svg" alt="Download on the App Store" />
                        </a>

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/google-play-badge.svg" alt="Get it on Google Play" />
                        </a>
                    </div>

                    <?php
                    $digital_magazine_image = get_field('digital_magazine_image', 'options');

                    if(!empty($digital_magazine_image)):
                    ?>
                        <div class="digital-magazine-cover">
                            <img src="<?php echo $digital_magazine_image['url']; ?>" alt="<?php echo $digital_magazine_image['alt']; ?>" class="img-responsive" />
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
