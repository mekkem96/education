<?php
/**
 * Displays main menu navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

    <nav class="nav-links">
        <?php wp_nav_menu( array(
            'theme_location' => 'main-menu',
            'menu_id'        => 'main-menu',
        ) ); ?>
    </nav>
