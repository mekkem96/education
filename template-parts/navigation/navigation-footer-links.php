<?php
/**
 * Displays footer links navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

    <nav class="nav-links">
        <?php wp_nav_menu( array(
            'theme_location' => 'footer-links',
            'menu_id'        => 'footer-links',
        ) ); ?>
    </nav>
