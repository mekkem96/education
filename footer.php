<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

    <footer class="content-wrapper footer_extra_class footer_extra_class3">
        <div class="footer-wrapper">
            <p>Extra text</p>
            <a href="">Extra link</a>
            <a href="<?php echo esc_url(home_url('/')); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/delicious-black.svg" alt="Delicious logo" class="footer-logo" />
            </a>

            <div class="social-links">
                <p>Stay in touch - and share</p>

                <div class="social-sharing-links">
                    <div class="row no-gutters">
                        <ul>
                            <?php if (get_field('facebook_url', 'options')): ?>
                                <li><a href="<?php the_field('facebook_url', 'options'); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <?php endif; ?>
                            <?php if (get_field('instagram_url', 'options')): ?>
                                <li><a href="<?php the_field('instagram_url', 'options'); ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            <?php endif; ?>
                            <?php if (get_field('twitter_url', 'options')): ?>
                                <li><a href="<?php the_field('twitter_url', 'options'); ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <?php endif; ?>
                            <?php if (get_field('pinterest_url', 'options')): ?>
                                <li><a href="<?php the_field('pinterest_url', 'options'); ?>" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                            <?php endif; ?>
                            <?php if (get_field('youtube_url', 'options')): ?>
                                <li><a href="<?php the_field('youtube_url', 'options'); ?>" target="_blank"><i class="fab fa-youtube"></i></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="content-block">
                <div class="footer-links">
                    <?php get_template_part('template-parts/navigation/navigation', 'footer-links'); ?>
                </div>

                <div class="eye-to-eye-copy">
                    <p>Delicious magazine is a part of Eye to Eye Media Ltd.</p>
                </div>
            </div>
        </div>
</footer>


<!-- Lost password Modal -->
<div class="remodal small" data-remodal-id="forgot-modal">
    <div class="remodal_close_wrap">
        <button data-remodal-action="close" class="remodal-close">Close</button>
    </div>
    <h2>Lost my password</h2>
    <p>
        Enter the email address associated with your<br> account, and we'll send you a link to reset your<br> password.
    </p>
    <div class="row remodal_form">
        <form name="lostpasswordform" id="lostpasswordform" action="<?php echo esc_url( network_site_url( 'wp-login.php?action=lostpassword', 'login_post' ) ); ?>" method="post">
            <div class="col-12">
                <label for="email">Email*</label>
            </div>
            <div class="col-12 col-md-8">
                <input type="email"  name="user_login" id="user_login" value="<?php echo esc_attr($user_login); ?>" placeholder="Type your email here">
            </div>
            <div class="col-12 col-md-4 text-center">
                <input type="submit"  name="wp-submit" id="wp-submit" value="<?php esc_attr_e('Send me'); ?>"  class="greenbutton">
            </div>
        </form>
    </div>
</div>
<!--// Lost password Modal -->

<!-- Email Sent Modal -->
<div class="remodal small" data-remodal-id="sent-modal">
    <div class="remodal_close_wrap">
        <button data-remodal-action="close" class="remodal-close">Close</button>
    </div>
    <h2>Email sent</h2>
    <p>
        If an account was found for this email address, <br>we've emailed you instructions to reset your <br>password.
    </p>
    <div class="row remodal_form">
        <div class="col-12">
            <a href="#" class="greenborder">I didn't receive it</a>
        </div>
    </div>
</div>
<!--// Email Sent Modal -->

<!-- Thanks Modal -->
<div class="remodal small" data-remodal-id="thanks-modal" id="thanks-modal" >
    <div class="remodal_close_wrap">
        <button data-remodal-action="close" class="remodal-close">Close</button>
    </div>
    <h2>Thank you for sign up to our newsletter</h2>
    <p>
        Now you can stay up to date with all the latest news, recipes and offers.
    </p>
    <div class="row remodal_form">
        <div class="col-12">
            <a href="#" class="greenbutton" data-remodal-action="confirm">Continue exploring</a>
        </div>
    </div>
</div>
<!--// Thanks Modal -->

<!-- Confirm Modal -->
<div class="remodal confirm" data-remodal-id="confirm-modal" id="confirm-modal" >
    <div class="remodal_close_wrap">
        <button data-remodal-action="close" class="remodal-close">Close</button>
    </div>
    <h2>Confirmation</h2>
    <p>
        We have sent you an activation link to:<br>
        <?php echo $_GET['user_email']; ?> please click this link to <br>activate your account..
    </p>
    <div class="row remodal_form">
        <div class="col-12">
            <a data-remodal-target="thanks-modal" href="#" class="greenbutton" data-remodal-action="confirm">Continue exploring</a>
        </div>
    </div>
</div>
<!--// Confirm Modal -->
<script type="text/javascript">

    $( "#wp-submit" ).click(function() {
//        console.log("dssss");


    });


</script>
<?php wp_footer(); ?>

</body>
</html>