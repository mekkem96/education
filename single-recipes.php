<?php
/**
 * The template for displaying all recipe posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();

/*global $post, $is_bookmark, $related_content, $related_content_acf, $postMetas;

$post_id = $post->ID;
$is_bookmark = true;
$related_content = get_the_related_field($post_id, 'related_content');
$related_content_acf = get_field('related_content_acf', $post_id);
$postMetas = get_post_meta($post_id);*/
?>

    <main class="main-content content">
        <div class="content-wrapper">
            <!-- Recipe: Overview -->
            <section id="recipe-overview">
                <!-- Recipe: Header -->
                <div class="recipe-header">
                    <div class="row no-gutters">
                        <div class="col-sm-12 col-md-4 order-md-last">
                            <?php if( get_field('sponsor_name') ): ?>
                                <div class="sponsor">
                                    <div class="sponsor-text">Sponsored by<br><?php the_field('sponsor_name'); ?></div>

                                    <div class="sponsor-logo">
                                        <?php
                                        $sponsor_logo = get_field('sponsor_logo');

                                        if(!empty($sponsor_logo)): ?>
                                            <img src="<?php echo $sponsor_logo['url']; ?>" alt="<?php echo $sponsor_logo['alt']; ?>" class="img-responsive" />
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="col">
                            <div class="recipe-heading text-standard">
                                <div class="related-collection">
                                    <?php
                                    // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                    $taxonomy = 'collections';
                                    $category = get_the_terms(get_the_ID(), $taxonomy);
                                    $cat_link = true;

                                    // If post has a category assigned.
                                    if ($category):
                                        $category_display = '';
                                        $category_link = '';

                                        if (class_exists('WPSEO_Primary_Term')):
                                            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                            $wpseo_primary_term = new WPSEO_Primary_Term('collections', get_the_ID());
                                            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                            $term = get_term($wpseo_primary_term);

                                            if (is_wp_error($term)):
                                                // Default to first category (not Yoast) if an error is returned
                                                $category_display = $category[0]->name;
                                                $category_link = get_bloginfo('url').'/'.'collections/'.$term->slug;
                                            else:
                                                // Yoast Primary category
                                                $category_display = $term->name;
                                                $category_link = get_term_link($term->term_id);
                                            endif;
                                        else:
                                            // Default, display the first category in WP's list of assigned categories
                                            $category_display = $category[0]->name;
                                            $category_link = get_term_link($category[0]->term_id);
                                        endif;

                                        // Display category
                                        if (!empty($category_display)):
                                            if ($cat_link == true && !empty($category_link)):
                                                echo '<a href="'.$category_link.'">'.$category_display.'</a>';
                                            else:
                                                echo '<span class="post-category">'.$category_display.'</span>';
                                            endif;
                                        endif;
                                    endif;
                                    ?>
                                </div>

                                <div class="recipe-title">
                                    <h1><?php the_title(); ?></h1>
                                </div>
                            </div>

                            <div class="visible-tablet">
                                <?php get_template_part('template-parts/page/content', 'recipe-stats'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Recipe: Header -->

                <!-- Recipe: Details -->
                <div class="recipe-detail-wrapper">
                    <div class="row no-gutters">
                        <div class="col-md-6">
                            <div class="recipe-image full-width">
                                <?php $caption = get_the_post_thumbnail_caption(); ?>

                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo $caption; ?>" />

                                <?php
                                if(!empty($caption)){//If description is not empty show the div
                                    echo '<div class="media-caption">'.$caption.'</div>';
                                }
                                ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="recipe-details">
                                <div class="hide-tablet">
                                    <?php get_template_part('template-parts/page/content', 'recipe-stats'); ?>
                                </div>

                                <div class="recipe-info">
                                    <div class="recipe-guidance">
                                        <ul class="list-guidance">
                                            <?php if (get_field('servings')): ?>
                                            <li>
                                                <span class="icon-inline icon-serves-green">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/serves-green.svg" alt="Serves icon" />
                                                </span>
                                                <span><?php the_field('servings'); ?></span>
                                            </li>
                                            <?php endif; ?>

                                            <?php if (get_field('recipe_time')): ?>
                                            <li>
                                                <span class="icon-inline icon-time-green">
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/time-green.svg" alt="Time icon" />
                                                </span>
                                                <span><?php the_field('recipe_time'); ?></span>
                                            </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>

                                    <div class="recipe-description text-standard" itemprop="description">
                                        <?php the_field('teaser_large'); ?>
                                    </div>

                                    <div class="recipe-labels nav-list">
                                        <ul>
                                            <?php
                                            $other_attributes = get_field('other_attributes');

                                            if($other_attributes): ?>
                                                <li>
                                                    <span class="icon-inline icon-freezable"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/freezable.svg" alt="Freezable icon" /></span>
                                                    <?php foreach($other_attributes as $other_attribute): ?>
                                                        <span><?php echo $other_attribute; ?></span>
                                                    <?php endforeach; ?>
                                                </li>
                                            <?php endif; ?>

                                            <?php
                                            $dietaries = get_field('dietary');

                                            if($dietaries): ?>
                                                <li>
                                                    <span class="icon-inline icon-gluten-free"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/gluten-free.svg" alt="Gluten free icon" /></span>
                                                    <?php foreach($dietaries as $dietary): ?>
                                                        <span><?php echo $dietary; ?></span>
                                                    <?php endforeach; ?>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>

                                    <?php
                                    $calories = get_field('nutritional_kcal');
                                    $fat = get_field('nutritional_fat');
                                    $protein = get_field('nutritional_protein');
                                    $carbs = get_field('nutritional_carbs');
                                    $fibre = get_field('nutritional_fibre');
                                    $salt = get_field('nutritional_salt');
                                    ?>

                                    <div id="nutrition" class="recipe-nutrition text-standard">
                                        <!-- Desktop Version -->
                                        <?php if ($calories || $fat || $protein || $carbs || $fibre || $salt): ?>
                                        <div class="hide-tablet">
                                            <h3 class="heading-alt">Nutrition: per serving</h3>

                                            <dl class="list-two-column style-lined">
                                                <?php if($calories): ?>
                                                    <dt>Calories</dt>
                                                    <dd><?php the_field('nutritional_kcal'); ?></dd>
                                                <?php endif; ?>

                                                <?php if($fat): ?>
                                                    <dt>Fat</dt>
                                                    <dd><?php the_field('nutritional_fat'); ?></dd>
                                                <?php endif; ?>

                                                <?php if($protein): ?>
                                                    <dt>Protein</dt>
                                                    <dd><?php the_field('nutritional_protein'); ?></dd>
                                                <?php endif; ?>

                                                <?php if($carbs): ?>
                                                    <dt>Carbohydrates</dt>
                                                    <dd><?php the_field('nutritional_carbs'); ?></dd>
                                                <?php endif; ?>

                                                <?php if($fibre): ?>
                                                    <dt>Fibre</dt>
                                                    <dd><?php the_field('nutritional_fibre'); ?></dd>
                                                <?php endif; ?>

                                                <?php if($salt): ?>
                                                    <dt>Salt</dt>
                                                    <dd><?php the_field('nutritional_salt'); ?></dd>
                                                <?php endif; ?>
                                            </dl>
                                        </div>
                                        <?php endif; ?>

                                        <?php if ($calories || $fat || $protein || $carbs || $fibre || $salt): ?>
                                        <!-- Mobile Accordion Version -->
                                        <div class="accordion visible-tablet">
                                            <a data-toggle="collapse" href="#nutritionContent" role="button" aria-expanded="false" aria-controls="nutritionContent">Nutrition: per serving</a>

                                            <div class="collapse" id="nutritionContent">
                                                <dl class="list-two-column style-lined">
                                                    <?php if($calories): ?>
                                                        <dt>Calories</dt>
                                                        <dd><?php the_field('nutritional_kcal'); ?></dd>
                                                    <?php endif; ?>

                                                    <?php if($fat): ?>
                                                        <dt>Fat</dt>
                                                        <dd><?php the_field('nutritional_fat'); ?></dd>
                                                    <?php endif; ?>

                                                    <?php if($protein): ?>
                                                        <dt>Protein</dt>
                                                        <dd><?php the_field('nutritional_protein'); ?></dd>
                                                    <?php endif; ?>

                                                    <?php if($carbs): ?>
                                                        <dt>Carbohydrates</dt>
                                                        <dd><?php the_field('nutritional_carbs'); ?></dd>
                                                    <?php endif; ?>

                                                    <?php if($fibre): ?>
                                                        <dt>Fibre</dt>
                                                        <dd><?php the_field('nutritional_fibre'); ?></dd>
                                                    <?php endif; ?>

                                                    <?php if($salt): ?>
                                                        <dt>Salt</dt>
                                                        <dd><?php the_field('nutritional_salt'); ?></dd>
                                                    <?php endif; ?>
                                                </dl>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Recipe: Details -->
            </section>
            <!-- /Recipe: Overview -->

            <!-- Recipe: Utilities - Mobile -->
            <section id="utilities" class="visible-tablet content-block">
                <div class="recipe-utilities full-width">
                    <div class="row no-gutters">
                        <div class="col">
                            <a href="" title="Save recipe" class="bttn bttn-icon-save">
                                <span></span>
                                Save recipe
                            </a>
                        </div>

                        <div class="col-auto">
                            <div class="sharing-links">
                                <?php echo do_shortcode('[ssba-buttons]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /Recipe: Utilities - Mobile -->

            <!-- Recipe: Utilities - Desktop-->
            <section id="utilities" class="hide-tablet content-block">
                <div class="recipe-utilities">
                    <div class="row no-gutters">
                        <div class="col">
                            <a href="" title="Save recipe" class="bttn bttn-icon-save">
                                <span></span>
                                Save recipe
                            </a>
                            <a href="" title="Print recipe" class="bttn bttn-icon-print">
                                <span></span>
                                Print
                            </a>
                        </div>

                        <div class="col-auto">
                            <div class="social-sharing-links">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <div class="sharing-title text-standard">
                                            <h4 class="heading-std-text">Share this recipe</h4>
                                        </div>
                                    </div>

                                    <div class="col-auto">
                                        <div class="sharing-links">
                                            <?php echo do_shortcode('[ssba-buttons]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /Recipe: Utilities - Desktop -->

            <!-- Recipe: Directions -->
            <section id="recipe-main">
                <div class="row no-gutters">
                    <div class="col-sm-12 col-md-8 col-lg-9">
                        <div class="recipe-directions content-block double-margin col-gutter">
                            <div class="hide-tablet">
                                <?php if (get_field('ingredients')): ?>
                                    <div class="recipe-ingredients text-standard">
                                        <h2>Ingredients</h2>
                                        <?php the_field('ingredients'); ?>
                                    </div>
                                <?php endif; ?>

                                <!-- Whisk Script -->
                                <script async="true" src="https://cdn.whisk.com/sdk/shopping-list.js" type="text/javascript"></script>
                                <script>
                                    var whisk = whisk || {};
                                    whisk.queue = whisk.queue || [];

                                    whisk.queue.push(function () {
                                        whisk.shoppingList.defineWidget("WWID-WKXK-TAUC-WHQD", {
                                            whiteLabel: "deliciouscouknew",
                                            styles: {
                                                size: "large",
                                                align: "left",
                                                linkColor: "#38C4A4",
                                                button: {
                                                    color: "#38C4A4",
                                                    text: "Add to shopping list"
                                                }
                                            }
                                        });
                                    });
                                </script>

                                <div id="WWID-WKXK-TAUC-WHQD" class="content-block">
                                    <script>
                                        whisk.queue.push(function () {
                                            whisk.display("WWID-WKXK-TAUC-WHQD");
                                        });
                                    </script>
                                </div>

                                <?php if (get_field('method')): ?>
                                    <div class="recipe-method text-standard content-block double-margin col-gutter">
                                        <h2>Method</h2>
                                        <?php the_field('method'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="visible-tablet">
                                <div class="row no-gutters">
                                    <!-- Advertisement -->
                                    <div class="col-12">
                                        <div class="advert-content content-block double-margin">
                                            <div class="row justify-content-center no-gutters">
                                                <div class="col">
                                                    <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                                    <div id='div-gpt-ad-1420540-2'>
                                                      <script>
                                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                                      </script>
                                                    </div>
                                                    <!-- End AdSlot 2 -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-ingredients-tab" data-toggle="tab" href="#nav-ingredients" role="tab" aria-controls="nav-ingredients" aria-selected="true">Ingredients</a>
                                        <a class="nav-item nav-link" id="nav-method-tab" data-toggle="tab" href="#nav-method" role="tab" aria-controls="nav-method" aria-selected="false">Method</a>
                                    </div>
                                </nav>

                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active full-width" id="nav-ingredients" role="tabpanel" aria-labelledby="nav-ingredients-tab">
                                        <div class="recipe-ingredients text-standard">
                                            <?php the_field('ingredients'); ?>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade full-width" id="nav-method" role="tabpanel" aria-labelledby="nav-method-tab">
                                        <div class="recipe-method text-standard">
                                            <h2>Method</h2>
                                            <?php the_field('method'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row no-gutters">
                                    <!-- Advertisement -->
                                    <div class="col-12">
                                        <div class="advert-content content-block double-margin">
                                            <div class="row justify-content-center no-gutters">
                                                <div class="col">
                                                    <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                                    <div id='div-gpt-ad-1420540-2'>
                                                      <script>
                                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                                      </script>
                                                    </div>
                                                    <!-- End AdSlot 2 -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php if(get_field('tips') || get_field('make_ahead') || get_field('know_how') || get_field('to_drink')): ?>
                            <div id="recipe-tips" class="recipe-tips text-standard content-block double-margin">
                                <h2>delicious. tips</h2>

                                <ol>
                                    <?php if(get_field('tips')): ?>
                                        <li class="tips"><?php the_field('tips'); ?></li>
                                    <?php
                                    endif;
                                    if(get_field('make_ahead')): ?>
                                        <li class="make-ahead"><?php the_field('make_ahead'); ?></li>
                                    <?php
                                    endif;
                                    if(get_field('know_how')): ?>
                                        <li class="know-how"><?php the_field('know_how'); ?></li>
                                    <?php
                                    endif;
                                    if(get_field('to_drink')): ?>
                                        <li class="to-drink"><?php the_field('to_drink'); ?></li>
                                    <?php endif; ?>
                                </ol>
                            </div>
                            <?php endif; ?>

                            <?php
                            // WINE RECOMMENDATION
                            if(get_field('wine_editors_choice')):
                            ?>
                                <div class="recipe-wine content-block double-margin">
                                    <div class="wine-wrapper">
                                        <h4>Wine recommendation</h4>
                                        <p><?php the_field('name_of_wine'); ?></p>
                                        <p><?php the_field('wine_description'); ?></p>

                                        <div class="wine-logo">
                                            <?php
                                            $wine_logo = get_field('wine_logo');

                                            if(!empty($wine_logo)):
                                            ?>
                                                <img src="<?php echo $wine_logo['url']; ?>" alt="<?php echo $wine_logo['alt']; ?>" />
                                            <?php endif; ?>
                                        </div>

                                        <div class="wine-image">
                                            <?php
                                            $wine_image = get_field('image_of_wine');

                                            if(!empty($wine_image)):
                                            ?>
                                                <img src="<?php echo $wine_image['url']; ?>" alt="<?php echo $wine_image['alt']; ?>" />
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?php
                            // RECIPE AUTHOR DETAILS
                            if(get_field('recipe_by')):
                            ?>
                                <div class="recipe-author-wrapper content-block double-margin">
                                    <div class="recipe-author-details">
                                        <p>Recipe By</p>
                                        <?php the_field('recipe_by'); ?>
                                    </div>

                                    <div class="magazine-subscription">
                                        <p>Subscribe</p>
                                        <p>Fancy getting a copy in print?</p>
                                        <a href="https://www.subscription.co.uk/EyeToEye/Delicious/Store/Subscription/DLC/IDLC0818" target="_blank" title="Subscribe to Delicious magazine">Subscribe to our magazine</a>

                                        <?php
                                        $postal_magazine_image = get_field('postal_magazine_image', 'options');

                                        if(!empty($postal_magazine_image)):
                                            ?>
                                            <div class="postal-magazine-cover">
                                                <img src="<?php echo $postal_magazine_image['url']; ?>" alt="<?php echo $postal_magazine_image['alt']; ?>" class="img-responsive" />
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="recipe-author-wrapper content-block double-margin">
                                    <div class="magazine-subscription full-width">
                                        <p>Subscribe</p>
                                        <p>Fancy getting a copy in print?</p>
                                        <a href="https://www.subscription.co.uk/EyeToEye/Delicious/Store/Subscription/DLC/IDLC0818" target="_blank" title="Subscribe to Delicious magazine">Subscribe to our magazine</a>

                                        <?php
                                        $postal_magazine_image = get_field('postal_magazine_image', 'options');

                                        if(!empty($postal_magazine_image)):
                                            ?>
                                            <div class="postal-magazine-cover">
                                                <img src="<?php echo $postal_magazine_image['url']; ?>" alt="<?php echo $postal_magazine_image['alt']; ?>" class="img-responsive" />
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="visible-tablet">
                                <div class="row no-gutters">
                                    <!-- Advertisement -->
                                    <div class="col-12">
                                        <div class="advert-content content-block double-margin">
                                            <div class="row justify-content-center no-gutters">
                                                <div class="col">
                                                    <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                                    <div id='div-gpt-ad-1420540-2'>
                                                      <script>
                                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                                      </script>
                                                    </div>
                                                    <!-- End AdSlot 2 -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="rate-review text-standard">
                                <h2>Rate &amp; review</h2>

                                <div class="rate">
                                    <p>Rate</p>
                                    <?php echo do_shortcode('[yasr_visitor_votes size="large" ]'); ?>
                                </div>

                                <div class="reviews">
                                    <p>Reviews</p>

                                    <?php
                                    /*$comment_form_args = array(
                                        'title_reply'       => __( '' ),
                                        'title_reply_to'    => __( 'Leave a review to %s' ),
                                        'cancel_reply_link' => __( 'Cancel Review' ),
                                        'label_submit'      => __( 'Submit' ),
                                        'format'            => 'xhtml',

                                        'comment_field' =>  '<p class="comment-form-comment"><label for="comment">'._x('', 'noun').'</label><textarea id="comment" name="comment" placeholder="Something to say? Tell us what you loved about this. Inspire others, get typing" cols="45" rows="8" aria-required="true">'.'</textarea></p>',

                                        'must_log_in' => '<p class="must-log-in">' .
                                            sprintf(
                                                __('You must be <a href="%s">logged in</a> to post a comment.'),
                                                wp_login_url(apply_filters('the_permalink', get_permalink()))
                                            ) . '</p>',

                                        'logged_in_as' => '<p class="logged-in-as">' .
                                            sprintf(
                                                __('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>'),
                                                admin_url('profile.php'),
                                                $user_identity,
                                                wp_logout_url(apply_filters('the_permalink', get_permalink()))
                                            ) . '</p>',
                                    );

                                    comment_form($comment_form_args);*/
                                    ?>
                                </div>

                                <?php
                                // If comments are open or we have at least one comment, load up the comment template.
                                if (comments_open() || get_comments_number()):
                                    ?>
                                    <div class="comment-wrapper">
                                        <?php comments_template('/comments-custom.php'); ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <?php // SIDEBAR ?>
                    <div class="sidebar col-sm-12 col-md-4 col-lg-3 hide-tablet">
                        <div class="side-top-ad">
                            <aside class="sidebar ad">
                                <div class="row no-gutters row-deep">
                                    <div class="col-12 align-self-start">
                                        <!-- Advertisement: Desktop > 1024 -->
                                        <div class="hide-tablet">
                                            <div class="advert-content content-block double-margin">
                                                <!-- Async AdSlot 3 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[300,600],[300,250]] -->
                                                <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[2]]) -->
                                                <div id='div-gpt-ad-1420540-3'>
                                                  <script>
                                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-3'); });
                                                  </script>
                                                </div>
                                                <!-- End AdSlot 3 -->
                                            </div>
                                        </div>
                                        <!-- /Advertisement: Desktop > 1024 -->
                                    </div>
                                </div>
                            </aside>
                        </div>
                        <?php
                        $related_how_about_recipes = get_field('how_about_related_recipes');

                        if($related_how_about_recipes):
                        ?>
                        <div class="col-12 align-self-end">
                            <div class="recipe-how-about content-block double-margin">
                                <div class="intro-text text-standard">
                                    <h2>How about...</h2>
                                </div>

                                <div class="cards card-stack">
                                    <?php
                                    foreach($related_how_about_recipes as $post): // variable must be called $post (IMPORTANT)
                                        setup_postdata($post);
                                    ?>
                                        <div class="card card-bg card-utilities">
                                            <div class="card-img-wrapper">
                                                <a href="#" class="cta-icon icon-only cta-icon-not-saved-white">
                                                    <span><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save recipe icon" /></span>
                                                </a>

                                                <a href="<?php the_permalink(); ?>">
                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top">
                                                </a>
                                            </div>

                                            <div class="card-body">
                                                <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>
                                            </div>

                                            <div class="card-footer">
                                                <div class="recipe-stats nav-list">
                                                    <ul>
                                                        <li class="recipe-rating">
                                                            <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                                                        </li>
                                                        <li class="recipe-skill">
                                                            <span class="icon-inline icon-effort-main">
                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                                                            </span>
                                                            <?php if(get_field('difficulty') == 'Easy'): ?>
                                                                <span>Easy</span>
                                                            <?php elseif (get_field('difficulty') == 'Medium'): ?>
                                                                <span>Medium</span>
                                                            <?php elseif (get_field('difficulty') == 'Challenge'): ?>
                                                                <span>A challenge</span>
                                                            <?php endif; ?>
                                                        </li>
                                                        <li class="recipe-comments">
                                                            <span class="icon-inline icon-comments-green">
                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                            </span>
                                                            <?php
                                                            $args = array(
                                                                'post_id' => get_the_ID(), // use post_id, not post_ID
                                                                'count' => true //return only the count
                                                            );
                                                            $comments_count = get_comments($args);
                                                            ?>
                                                            <span><?php echo $comments_count; ?></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    endforeach;
                                    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?php
                        $related_food_articles = get_field('related_articles');

                        $args = array(
                            'post_id' => get_the_ID(), // use post_id, not post_ID
                            'count' => true //return only the count
                        );
                        $comments_count = get_comments($args);

                        if($related_food_articles):
                        ?>
                        <div class="side-bot-ad">
                            <aside class="sidebar ad">
                                <div class="row no-gutters row-deep">
                                    <div class="col-12 align-self-start">
                                        <!-- Advertisement: Desktop > 1024 -->
                                        <div class="hide-tablet">
                                            <div class="advert-content content-block double-margin">
                                                <!-- Async AdSlot 3 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[300,600],[300,250]] -->
                                                <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[2]]) -->
                                                <div id='div-gpt-ad-1420540-3' style="background-color: black;display: block;height: 300px;">
                                                  <script>
                                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-3'); });
                                                  </script>
                                                </div>
                                                <!-- End AdSlot 3 -->
                                            </div>
                                        </div>
                                        <!-- /Advertisement: Desktop > 1024 -->
                                    </div>
                                </div>
                            </aside>
                        </div>

                        <div class="col-12 align-self-end">
                            <div class="related-articles content-block double-margin">
                                <div class="intro-text text-standard">
                                    <h2>More food for thought...</h2>
                                </div>

                                <div <?php post_class('cards card-stack'); ?>>
                                    <?php
                                    foreach($related_food_articles as $post): // variable must be called $post (IMPORTANT)
                                        setup_postdata($post);
                                        ?>
                                        <div class="card card-utilities">
                                            <?php
                                            $post_categories = get_post_primary_category($post->ID, 'category');
                                            $primary_category = $post_categories['primary_category'];
                                            ?>
                                            <div class="card-img-wrapper">
                                                <a href="<?php the_permalink(); ?>">
                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top">
                                                </a>
                                            </div>

                                            <div class="card-body">
                                                <p><?php echo $primary_category->name; ?></p>

                                                <h4 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                                <?php if (has_excerpt()): ?>
                                                    <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 15, $more = '...'); ?>
                                                    <p><?php echo $excerpt_text; ?></p>
                                                <?php endif; ?>
                                            </div>

                                            <div class="card-footer">
                                                <div class="article-stats nav-list">
                                                    <ul>
                                                        <li class="article-author"><?php the_author(); ?></li>
                                                        <li class="article-date"><?php the_date('M y'); ?></li>
                                                        <li class="article-comments">
                                                            <span class="icon-inline icon-comments-green">
                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                            </span>
                                                            <span><?php echo $comments_count; ?></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                        wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
            
            <section id="social-feed" class="content-block double-margin">
                <div class="row no-gutters">
                    <div class="col-md-12">
                        <div class="intro-text text-standard">
                            <?php
                            $social_feed_title = get_field('social_feed_title', 'options');
                            $social_feed_text = get_field('social_feed_text', 'options');
                            ?>
                            <h2><?php echo $social_feed_title; ?></h2>
                        </div>

                        <div class="col-12">
                            <?php echo $social_feed_text; ?>
                        </div>
                    </div>

                    <div class="col-12">
                        <!-- Place <div> tag where you want the feed to appear -->
                        <div id="curator-feed"><a href="https://curator.io" target="_blank" class="crt-logo crt-tag">Powered by Curator.io</a></div>
                        <!-- The Javascript can be moved to the end of the html page before the </body> tag -->
                        <script type="text/javascript">
                        /* curator-feed */
                        (function(){
                        var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
                        i.src = "https://cdn.curator.io/published/c52de8ab-e8c9-46ff-b504-759acf83aa18.js";
                        e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);
                        })();
                        </script>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <!-- Advertisement -->
                    <div class="col-12">
                        <div class="advert-content content-block double-margin">
                            <div class="row justify-content-center no-gutters">
                                <div class="col">
                                    <!-- Async AdSlot 4 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[3]]) -->
                                    <div id='div-gpt-ad-1420540-4'>
                                          <script>
                                            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-4'); });
                                          </script>
                                    </div>
                                    <!-- End AdSlot 4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <div class="col-12">
                        <div class="content-block double-margin">
                            <div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="AR_1" data-ob-template="RadioTimes"></div>
                            <script type="text/javascript" async="async" src="https://widgets.outbrain.com/outbrain.js"></script>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

    <?php
    global $wpdb, $postMetas, $post;
    $post_id = $post->ID;

    $rating = $wpdb->get_row("SELECT number_of_votes, sum_votes from wp_yasr_votes where post_id=$post->ID", ARRAY_N, 0);
    $rate_count = $rating[0] != NULL ? $rating[0] : 1;
    $rate_total = $rating[0] != NULL ? $rating[1] : 5;
    $rate_average = $rate_count == 0 ? 0 : round($rate_total / $rate_count, 1);
    ?>

    <?php /*
    global $wpdb, $postMetas, $post;
    $post_id = $post->ID;
    $seo_imageArr = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', true);
    $seo_image = $seo_imageArr[0];

    $rating = $wpdb->get_row("SELECT number_of_votes, sum_votes from wp_yasr_votes where post_id=$post->ID", ARRAY_N, 0);
    $rate_count = $rating[0] != NULL ? $rating[0] : 1;
    $rate_total = $rating[0] != NULL ? $rating[1] : 5;
    $rate_average = $rate_count == 0 ? 0 : round($rate_total / $rate_count, 1);
    $ingredientsInformation = get_the_field_value(maybe_unserialize($postMetas['ingredients'][0]));
    $origin_array = array('<ul>', '</ul>', '<li>', '<li class="p1">', '<li class="p2">', '<li class="p3">','</li>', '<p>&nbsp;</p>', '<p></p>', '<p> </p>', '<b>', '</b>', '<p>', '<p class="p1">', '<p class="p2">', '<p class="p3">', '</p>');
    $new_array   = array('', '', '"', '"', '"', '"', '",', '', '', '', '', '', '"', '"', '"', '"', '",');
    $ingredientsInformationNew = str_replace($origin_array, $new_array, $ingredientsInformation);
    */ ?>

    <script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Recipe",
        "name": "<?php the_title(); ?>",
        "image": "<?php echo get_the_post_thumbnail_url(); ?>",
        "description": "<?php the_field('teaser_large'); ?>",
        "cookTime": "<?php the_field('recipe_time'); ?>",
        "totalTime": "<?php the_field('recipe_time'); ?>",
        "recipeYield": "<?php the_field('servings'); ?>",
        "recipeIngredient": [<?php  echo $ingredientsInformationNew . '"Ingredient ending"'; ?>],
        "nutrition": {
            "@type": "NutritionInformation",
            "calories": "<?php the_field('nutritional_kcal'); ?>",
            "fatContent": "<?php the_field('nutritional_fat'); ?>",
            "proteinContent": "<?php the_field('nutritional_protein'); ?>",
            "carbohydrateContent": "<?php the_field('nutritional_carbs'); ?>",
            "fiberContent": "<?php the_field('nutritional_fibre'); ?>"
        },
        "recipeInstructions": [
            {
                "@type": "HowToStep",
                "text": "<?php the_field('method'); ?>"
            }
        ],
        "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "<?php echo $rate_average; ?>",
            "reviewCount": "<?php echo $rate_count; ?>"
		}
    }
    </script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                //    event.preventDefault();
                var page = 2;

                $('.bttn-icon-save').click(function(e) {
                    e.defaultPrevented = false;
                    return false;
                });
                $('.cta-icon-not-saved-white').click(function(e) {
                    e.defaultPrevented = false;
                    return false;
                });

                $('a.bttn-icon-save').click( function(e){
                    var post_id = $(this).attr('post_id');
                    var user_id = $(this).attr('user_id');
                    var field = 'recipes';
                    $(this).parent().parent().addClass('recipe-card--loading');
                    console.log(post_id);
                    console.log(user_id);
                    console.log(field);
                    $.ajax({
                        url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                        data: {
                            'action': 'get_recipes_save',
                            'post_id' : post_id,
                            'user_id' : user_id
                            //'field' : field,
                        },
                        success:function(data) {
                            console.log(data);
                            console.log(post_id);
                            $('.btn-' + post_id).parent().parent().removeClass('recipe-card--loading');
                            $('a.bttn-icon-save img').attr('src', '<?= esc_url(home_url('/wp-content/themes/delicious/assets/icons/saved-white.svg')); ?>');
                        }
                    });
                });

                $('.cta-icon-not-saved-white').click(function(e) {
                    var post_id = $(this).attr('post_id');
                    var user_id = $(this).attr('user_id');
                    var field = 'recipes';
                    $(this).parent().parent().addClass('recipe-card--loading');
                    console.log(post_id);
                    console.log(user_id);
                    console.log(field);
                    $.ajax({
                        url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                        data: {
                            'action': 'get_recipes_save',
                            'post_id' : post_id,
                            'user_id' : user_id
                            //'field' : field,
                        },
                        success:function(data) {
                            console.log(data);
                            console.log(post_id);
                            $('.btn-' + post_id).parent().parent().removeClass('recipe-card--loading');
                            $('.btn-' + post_id)
                                .find('img').attr('src', '<?= esc_url(home_url('/wp-content/themes/delicious/assets/icons/saved-white.svg')) ?>');
                        }
                    });
                });


            })
        })(jQuery);
    </script>

<?php get_footer();
