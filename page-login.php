<?php
/**
 * Template Name: Login
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <div class="t_header_placeholder"></div>
    <div class="wrap nopad t_register_form">
        <div class="row">
            <div class="col-12 col-lg-6 ">
                <form name="loginform" id="loginform" action="<?php echo esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" method="post">
                    <div class="row t_form_styler left">
                        <div class="col-12">
                            <h2>Log in</h2>
                        </div>
                        <div class="col-12 t_input_line">
                            <label for="email">Email*</label>
                            <input type="text" name="log" id="user_login"<?php echo $aria_describedby_error; ?> class="input" value="<?php echo esc_attr( $user_login ); ?>" >
                        </div>
                        <div class="col-12">
                            <label for="password">Password*</label>
                            <input type="password" name="pwd" id="user_pass"<?php echo $aria_describedby_error; ?> class="input" value="" >
                        </div>
                        <div class="col-12 t_remember">
                            <div class="t_radio">
                                <label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever" <?php checked( $rememberme ); ?> /> <?php esc_html_e( 'Remember Me' ); ?></label>
                            </div>
                            <a href="#forgot-modal" class="t_forgot">I forgot my password</a>
                        </div>
                        <div class="col-12 text-center">
                            <input type="submit" name="wp-submit" id="wp-submit" value="<?php esc_attr_e('Log In'); ?>" class="greenbutton">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-12 col-lg-6">
                <div class="row t_form_styler text-center right">
                    <div class="col-12">
                        <label>Or log in with your socials</label>
                        <div>
                            <a href="javascript:void(0)" class="t_facebook">Register with facebook</a>
                        </div>
                        <script type="text/javascript">
                            $( ".t_facebook" ).click(function() {
                                $( ".bp_social_connect_facebook" ).trigger( "click" );
                            });
                        </script>
                        <div>
                            <a href="https://accounts.google.com/o/oauth2/auth?response_type=code&amp;redirect_uri=http%3A%2F%2Fdeliciousnew.wpengine.com&amp;client_id=598740094483-7j4ad94valanrecfpvvbuljgojh8n09f.apps.googleusercontent.com&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&amp;access_type=offline&amp;approval_prompt=force" class="t_gmail">Register with gmail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .wrap -->


    <input type="hidden" id="bp_social_connect_security" name="bp_social_connect_security" value="984d7d2c0d" /><input type="hidden" name="_wp_http_referer" value="/wp-login.php?loggedout=true" />		<script type="text/javascript">
    var ajaxurl = 'http://delicious.1devserver.co.uk/wp-admin/admin-ajax.php';
</script>
    <style>
        .bp_social_connect_facebook , .bp_social_connect_google {
            display: none;
        }
        .t_facebook {
            cursor: pointer;
        }
    </style>
    <div class="bp_social_connect">		<div id="fb-root" class="bp_social_connect_fb"></div>
        <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : "269590670319265", // replace your app id here
                    status     : true,
                    cookie     : true,
                    xfbml      : true,
                    version    : 'v2.0'
                });
                FB.Event.subscribe('auth.authResponseChange', function(response){

                    if (response.status === 'connected'){
                        console.log('success');
                    }else if (response.status === 'not_authorized'){
                        console.log('failed');
                    } else{
                        console.log('unknown error');
                    }
                });
            };
            (function(d){
                var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement('script'); js.id = id; js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document));
            jQuery(document).ready(function($){
                $('.bp_social_connect_facebook').unbind('click');
                $('.bp_social_connect_facebook').on('click',function(){
                    var $this = $(this);
                    $this.addClass('loading');
                    var security = $('#bp_social_connect_security').val();
                    FB.login(function(response){
                        if (response.authResponse){

                            FB.api('/me', function(response) {
                                $.ajax({
                                    url: ajaxurl,
                                    data: 'action=bp_social_connect_facebook_login&id='+response.id+'&email='+response.email+'&first_name='+response.first_name+'&last_name='+response.last_name+'&gender='+response.gender+'&name='+response.name+'&link='+response.link+'&locale='+response.locale+'&security='+security,
                                    type: 'POST',
                                    dataType: 'JSON',
                                    success:function(data){
                                        $this.removeClass('loading');
                                        console.log(data);
                                        if (data.redirect_uri){
                                            if (data.redirect_uri =='refresh') {
                                                window.location.href =jQuery(location).attr('href');
                                            } else {
                                                window.location.href = data.redirect_uri;
                                            }
                                        }else{
                                            window.location.href = "http://delicious.1devserver.co.uk";
                                        }
                                    },
                                    error: function(xhr, ajaxOptions, thrownError) {
                                        $this.removeClass('loading');
                                        window.location.href = "http://delicious.1devserver.co.uk";
                                    }
                                });

                            });
                        }else{

                        }
                    }, {scope: 'email,user_likes', return_scopes: true});
                });
            });
        </script>
        <a class="bp_social_connect_facebook" href="javascript:void(0)">FACEBOOK</a><a class="bp_social_connect_google" href="https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=http%3A%2F%2Fdeliciousnew.wpengine.com&client_id=598740094483-7j4ad94valanrecfpvvbuljgojh8n09f.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&access_type=offline&approval_prompt=force">GOOGLE</a><style>.bp_social_connect { display: inline-block; width: 100%; }
            .bp_social_connect_facebook{background:#3b5998;}.bp_social_connect_google{background:#DD4B39 !important;}.bp_social_connect > a{text-align:center;float:left;padding:15px;border-radius:2px;color:#fff !important;width:200px;margin:0 5px;}.bp_social_connect > a:first-child{margin-left:0;}
            .bp_social_connect > a:before{float:left;font-size:16px;font-family:fontawesome;opacity:0.6;}.bp_social_connect_facebook:before{content:"\f09a";}.bp_social_connect_google:before{content:"\f0d5";}</style></div>	<script type="text/javascript">
    var elm_button_vars = { wrapper: '#recipe_list' };
</script>




<?php get_footer();