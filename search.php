<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">

	<header class="page-header">
		<?php if ( have_posts() ) : ?>
			<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyseventeen' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php else : ?>
			<h1 class="page-title"><?php _e( 'Nothing Found', 'twentyseventeen' ); ?></h1>
		<?php endif; ?>
	</header><!-- .page-header -->

	<div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <?php

                /*$query = new WP_Query(
                    array(
                        's' => get_search_query(),
                        'post_type'         =>  array('recipe', 'post', 'taxonomy'),
                        'order'             =>  'ASC',
                        'posts_per_page'    =>  12
                    ));*/
                ?>
                <div class="t_header_placeholder"></div>
                <div class="top_page row">
                    <div class="col-12 col-md-9">
                        <h1>Results for “<?= get_search_query() ?>”</h1><small><?= $GLOBALS['wp_query']->post_count; ?> <?= ($GLOBALS['wp_query']->post_count > 1)? 'Results': 'Result'; ?></small>
                    </div>
                    <div class="col-12 col-md-3 filter_box">
                        <div>
                            <p>Filter by</p>
                            <div id="filters" class="button-group">
                                <button class="button" data-filter=".recipes">recipes <span>(0)</span></button>
                                <button class="button" data-filter=".collections">collections <span>(0)</span></button>
                                <button class="button" data-filter=".articles">articles <span>(0)</span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if ( have_posts() ) :
                    $postCounts = array(0,0,0);
                    /* Start the Loop */
                    echo '<div class="search_content"><div class="row search_grid">';
                    while ( have_posts() ) : the_post();
                        $isotopeClass = '';
                        if ( is_tax('collections') ) {
                            $postCounts[1] = $postCounts[1]+1;
                            $isotopeClass = 'collections';
                        } else if (get_post_type() == 'recipe') {
                            $isotopeClass = 'recipes';
                            $postCounts[0] = $postCounts[0]+1;
                        } else {
                            $isotopeClass = 'articles';
                            $postCounts[2] = $postCounts[2]+1;
                        }

                        echo '<div class="col-md-4 col-sm-6 col-xs-12 search_item '.$isotopeClass.'"><div>';
                        $image = get_the_post_thumbnail_url(get_the_ID(), 'medium');
                        echo '<div class="item_img" style="background: url(' . $image . ') no-repeat center center;background-size: cover;"></div>';
                        echo '<div class="item_title"><h5><a href="'.get_permalink().'">' . get_the_title() . '</a></h5>';
                        echo '<div class="item_attr row">';

                        if (is_tax('collections')) {
                            echo '<div class="col-12 col-sm-12 item_recipes">421 recipes</div>';
                        }

                        if (get_post_type() == 'recipe') {
                            $stars = (float) yasr_get_overall_rating(get_the_ID());
                            echo '<div class="col-sm-6 item_stars"><span class="stars_active" style="width:'.($stars*18).'px;"></span><span class="stars_passive"></span></div>';
                            echo '<div class="col-sm-6 item_score"><span class="icon-inline icon-effort-main">'
                                .'<img src="' . get_template_directory_uri() .'/assets/icons/effort-green.svg" alt="Effort icon" />';
                            echo '</span>';
                            if(get_field('difficulty') == 'Easy') :
                                echo '<span>Easy</span>';
                            elseif (get_field('difficulty') == 'Medium') :
                                echo '<span>Medium</span>';
                            elseif (get_field('difficulty') == 'Challenge'):
                                echo '<span>A challenge<span>';
                            endif;

                            echo '</div>';
                        }

                        echo '</div>';
                        echo '</div>';
                        echo '</div><div class="clearfix"></div></div>';

                    endwhile; // End of the loop.
                    echo '</div></div>'; ?>
                    <script>
                        (function($){
                            var counts = [<?= $postCounts[0] . ',' . $postCounts[1] . ',' . $postCounts[2]; ?>];
                            $('#filters button').each(function(i){
                                $(this).find('span').html('('+counts[i]+')');
                            });
                        })(jQuery);
                    </script>
                <?php

                else : ?>

                    <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' ); ?></p>
                    <?php

                endif;
                wp_reset_postdata(); ?>

            </main><!-- #main -->
        </div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
