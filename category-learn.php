<?php
/**
 * The category archive template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section>
                <div class="row no-gutters">
                    <div class="col-sm-12 col-md-6">
                        <div class="recipe-heading text-standard">
                            <div class="recipe-title">
                                <h1><?php single_cat_title(); ?></h1>
                                <?php echo category_description(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col order-md-last">
                        <?php if(get_field('sponsor_name', $term)): ?>
                            <div class="sponsor">
                                <div class="sponsor-text">Sponsored by<br><?php the_field('sponsor_name', $term); ?></div>

                                <div class="sponsor-logo">
                                    <?php
                                    $sponsor_logo = get_field('sponsor_logo', $term);

                                    if(!empty($sponsor_logo)): ?>
                                        <img src="<?php echo $sponsor_logo['url']; ?>" alt="<?php echo $sponsor_logo['alt']; ?>" class="img-responsive" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </section>

            <section id="category-main">
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-lg-9">
                        <?php
                        $category = get_category(get_query_var('cat'));
                        $cat_id = $category->cat_ID;

                        $term_id = $cat_id;
                        $taxonomy_name = 'category';
                        $term_children = get_term_children($term_id, $taxonomy_name);

                        $args = array(
                            'post_id' => get_the_ID(), // use post_id, not post_ID
                            'count' => true //return only the count
                        );
                        $comments_count = get_comments($args);
                        ?>

                        <div class="category-wrapper">
                            <div class="child-category content-block double-margin">
                                <h2><?php echo get_cat_name($category_id = 369);?></h2>
                                <p><?php echo category_description(369); ?></p>

                                <?php
                                // IN THE KITCHEN
                                $terms_args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => '4',
                                    'post_status' => 'publish',
                                    'cat' => 369
                                );
                                $query = new WP_Query($terms_args);

                                if ($query->have_posts()):
                                    ?>
                                    <div class="child-category-posts">
                                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                                            <a href="<?php the_permalink(); ?>" <?php post_class('card card-utilities'); ?>>
                                                <div class="card-img-wrapper">
                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/podcast-icon.svg" alt="Podcast icon" class="podcast-icon" />

                                                    <?php if (get_field('sponsor_name')): ?>
                                                        <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="card-body">
                                                    <?php
                                                    // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                                    $taxonomy = 'category';
                                                    $category = get_the_terms(get_the_ID(), $taxonomy);

                                                    // If post has a category assigned.
                                                    if ($category):
                                                        $category_display = '';

                                                        if (class_exists('WPSEO_Primary_Term')):
                                                            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                                            $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_ID());
                                                            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                                            $term = get_term($wpseo_primary_term);

                                                            if (is_wp_error($term)):
                                                                // Default to first category (not Yoast) if an error is returned
                                                                $category_display = $category[0]->name;
                                                            else:
                                                                // Yoast Primary category
                                                                $category_display = $term->name;
                                                            endif;
                                                        else:
                                                            // Default, display the first category in WP's list of assigned categories
                                                            $category_display = $category[0]->name;
                                                        endif;

                                                        echo '<p class="article-cat">'.$category_display.'</p>';
                                                    endif;
                                                    ?>

                                                    <p class="podcast-text">Delicious. Podcast</p>

                                                    <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                    <?php if (has_excerpt()): ?>
                                                        <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 20, $more = '...'); ?>
                                                        <p><?php echo $excerpt_text; ?></p>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="card-footer">
                                                    <div class="article-stats nav-list">
                                                        <ul>
                                                            <li class="article-author"><?php the_author(); ?></li>
                                                            <li class="article-date"><?php the_date(); ?></li>
                                                            <li class="article-comments">
                                                                <span class="icon-inline icon-comments-green">
                                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                                </span>
                                                                <span><?php echo $comments_count; ?></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php endwhile; ?>
                                    </div>
                                <?php else: ?>
                                    <p>There are currently no recipes for this collection.</p>
                                <?php
                                endif;

                                wp_reset_query();
                                ?>

                                <div class="cta">
                                    <a href="<?php echo $term->slug; ?>" class="bttn justify-content-center">More <?php echo $term->name; ?></a>
                                </div>
                            </div>

                            <div class="child-category content-block double-margin">
                                <h2><?php echo get_cat_name($category_id = 358);?></h2>
                                <p><?php echo category_description(358); ?></p>

                                <?php
                                // IN THE GARDEN
                                $terms_args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => '4',
                                    'post_status' => 'publish',
                                    'cat' => 358
                                );
                                $query = new WP_Query($terms_args);

                                if ($query->have_posts()):
                                    ?>
                                    <div class="child-category-posts">
                                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                                            <a href="<?php the_permalink(); ?>" <?php post_class('card card-utilities'); ?>>
                                                <div class="card-img-wrapper">
                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/podcast-icon.svg" alt="Podcast icon" class="podcast-icon" />

                                                    <?php if (get_field('sponsor_name')): ?>
                                                        <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="card-body">
                                                    <?php
                                                    // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                                    $taxonomy = 'category';
                                                    $category = get_the_terms(get_the_ID(), $taxonomy);

                                                    // If post has a category assigned.
                                                    if ($category):
                                                        $category_display = '';

                                                        if (class_exists('WPSEO_Primary_Term')):
                                                            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                                            $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_ID());
                                                            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                                            $term = get_term($wpseo_primary_term);

                                                            if (is_wp_error($term)):
                                                                // Default to first category (not Yoast) if an error is returned
                                                                $category_display = $category[0]->name;
                                                            else:
                                                                // Yoast Primary category
                                                                $category_display = $term->name;
                                                            endif;
                                                        else:
                                                            // Default, display the first category in WP's list of assigned categories
                                                            $category_display = $category[0]->name;
                                                        endif;

                                                        echo '<p class="article-cat">'.$category_display.'</p>';
                                                    endif;
                                                    ?>

                                                    <p class="podcast-text">Delicious. Podcast</p>

                                                    <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                    <?php if (has_excerpt()): ?>
                                                        <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 20, $more = '...'); ?>
                                                        <p><?php echo $excerpt_text; ?></p>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="card-footer">
                                                    <div class="article-stats nav-list">
                                                        <ul>
                                                            <li class="article-author"><?php the_author(); ?></li>
                                                            <li class="article-date"><?php the_date(); ?></li>
                                                            <li class="article-comments">
                                                                    <span class="icon-inline icon-comments-green">
                                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                                    </span>
                                                                <span><?php echo $comments_count; ?></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php endwhile; ?>
                                    </div>
                                <?php else: ?>
                                    <p>There are currently no recipes for this collection.</p>
                                <?php
                                endif;

                                wp_reset_query();
                                ?>

                                <div class="cta">
                                    <a href="<?php echo $term->slug; ?>" class="bttn justify-content-center">More <?php echo $term->name; ?></a>
                                </div>
                            </div>

                            <?php
                            /*foreach ($term_children as $child):
                                $term = get_term_by('id', $child, $taxonomy_name);
                                ?>
                                <div class="child-category content-block double-margin">
                                    <h2><?php echo $term->name; ?></h2>
                                    <p><?php echo $term->description; ?></p>

                                    <?php
                                    $terms_args = array(
                                        'cat' => $term->term_id,
                                        'post_type' => 'post',
                                        'posts_per_page' => '4',
                                    );
                                    $query = new WP_Query($terms_args);

                                    if ($query->have_posts()):
                                    ?>
                                        <div class="child-category-posts">
                                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                                <div class="card card-utilities">
                                                    <div class="card-img-wrapper">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top">
                                                        </a>
                                                    </div>

                                                    <div class="card-body">
                                                        <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>
                                                    </div>

                                                    <div class="card-footer">
                                                        <div class="article-stats nav-list">
                                                            <ul>
                                                                <li class="article-author"><?php echo $author; ?></li>
                                                                <li class="article-date"><?php the_date(); ?></li>
                                                                <li class="article-comments">
                                                                    <span class="icon-inline icon-comments-green">
                                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                                    </span>
                                                                    <span><?php echo $comments_count; ?></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    <?php else: ?>
                                        <p>There are currently no recipes for this collection.</p>
                                    <?php
                                    endif;

                                    wp_reset_query();
                                    ?>

                                    <div class="cta">
                                        <a href="<?php echo $term->slug; ?>" class="bttn justify-content-center">More <?php echo $term->name; ?></a>
                                    </div>
                                </div>
                            <?php endforeach;*/ ?>
                        </div>
                    </div>

                    <?php // SIDEBAR ?>
                    <div class="col-sm-12 col-md-4 col-lg-3 hide-tablet">
                        <aside class="sidebar">
                            <div class="row no-gutters row-deep">
                                <div class="col-12 align-self-start">
                                    <!-- Advertisement: Desktop > 1024 -->
                                    <div class="hide-tablet">
                                        <div class="advert-content content-block double-margin">
                                            <!-- Async AdSlot 3 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[300,600],[300,250]] -->
                                            <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[2]]) -->
                                            <div id='div-gpt-ad-1420540-3'>
                                              <script>
                                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-3'); });
                                              </script>
                                            </div>
                                            <!-- End AdSlot 3 -->
                                        </div>
                                    </div>
                                    <!-- /Advertisement: Desktop > 1024 -->
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <!-- Advertisement -->
                    <div class="col-12">
                        <div class="advert-content content-block double-margin">
                            <div class="row justify-content-center no-gutters">
                                <div class="col">
                                    <!-- Async AdSlot 4 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[3]]) -->
                                    <div id='div-gpt-ad-5444058-4'>
                                        <script>
                                            googletag.cmd.push(function() { googletag.display('div-gpt-ad-5444058-4'); });
                                        </script>
                                    </div>
                                    <!-- End AdSlot 4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php // COOKERY SCHOOL SLIDER ?>
            <section>
                <div class="content-block">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="intro-text text-standard">
                                <h2><?php echo get_cat_name($category_id = 1038);?></h2>
                                <div class="col-sm-12 col-md-5">
                                    <?php echo category_description(1038); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="category-slider">
                                <?php
                                $args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => '-1',
                                    'post_status' => 'publish',
                                    'cat' => 1038
                                );
                                $the_query = new WP_Query($args);

                                if ($the_query->have_posts()):
                                    while ($the_query->have_posts()) : $the_query->the_post();
                                        ?>
                                        <div <?php post_class('category-card'); ?>>
                                            <a href="<?php the_permalink(); ?>" class="card-utilities">
                                                <div class="card-img-wrapper">
                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/podcast-icon.svg" alt="Podcast icon" class="podcast-icon" />

                                                    <?php if (get_field('sponsor_name')): ?>
                                                        <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="card-body">
                                                    <?php
                                                    // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                                    $taxonomy = 'category';
                                                    $category = get_the_terms(get_the_ID(), $taxonomy);

                                                    // If post has a category assigned.
                                                    if ($category):
                                                        $category_display = '';

                                                        if (class_exists('WPSEO_Primary_Term')):
                                                            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                                            $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_ID());
                                                            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                                            $term = get_term($wpseo_primary_term);

                                                            if (is_wp_error($term)):
                                                                // Default to first category (not Yoast) if an error is returned
                                                                $category_display = $category[0]->name;
                                                            else:
                                                                // Yoast Primary category
                                                                $category_display = $term->name;
                                                            endif;
                                                        else:
                                                            // Default, display the first category in WP's list of assigned categories
                                                            $category_display = $category[0]->name;
                                                        endif;

                                                        echo '<p class="article-cat">'.$category_display.'</p>';
                                                    endif;
                                                    ?>

                                                    <p class="podcast-text">Delicious. Podcast</p>

                                                    <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                    <?php if (has_excerpt()): ?>
                                                        <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 20, $more = '...'); ?>
                                                        <p><?php echo $excerpt_text; ?></p>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="card-footer">
                                                    <div class="article-stats nav-list">
                                                        <ul>
                                                            <li class="article-author"><?php the_author(); ?></li>
                                                            <li class="article-date"><?php the_date(); ?></li>
                                                            <li class="article-comments">
                                                            <span class="icon-inline icon-comments-green">
                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                            </span>
                                                                <span><?php echo $comments_count; ?></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php
                                    endwhile;

                                    wp_reset_postdata();
                                else:
                                    ?>
                                    <p><?php esc_html_e('Sorry, there are currently no cookery school articles.'); ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php // ABOUT WINE SLIDER ?>
            <section>
                <div class="content-block double-margin">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="intro-text text-standard">
                                <h2><?php echo get_cat_name($category_id = 1039);?></h2>
                                <div class="col-sm-12 col-md-5">
                                    <?php echo category_description(1039); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="category-slider">
                                <?php
                                $args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => '-1',
                                    'post_status' => 'publish',
                                    'cat' => 1039
                                );
                                $the_query = new WP_Query($args);

                                if ($the_query->have_posts()):
                                    while ($the_query->have_posts()) : $the_query->the_post();
                                        ?>
                                        <div <?php post_class('category-card'); ?>>
                                            <a href="<?php the_permalink(); ?>" class="card-utilities">
                                                <div class="card-img-wrapper">
                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top">

                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/podcast-icon.svg" alt="Podcast icon" class="podcast-icon" />
                                                </div>

                                                <div class="card-body">
                                                    <?php
                                                    // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                                    $taxonomy = 'category';
                                                    $category = get_the_terms(get_the_ID(), $taxonomy);

                                                    // If post has a category assigned.
                                                    if ($category):
                                                        $category_display = '';

                                                        if (class_exists('WPSEO_Primary_Term')):
                                                            // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                                            $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_ID());
                                                            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                                            $term = get_term($wpseo_primary_term);

                                                            if (is_wp_error($term)):
                                                                // Default to first category (not Yoast) if an error is returned
                                                                $category_display = $category[0]->name;
                                                            else:
                                                                // Yoast Primary category
                                                                $category_display = $term->name;
                                                            endif;
                                                        else:
                                                            // Default, display the first category in WP's list of assigned categories
                                                            $category_display = $category[0]->name;
                                                        endif;

                                                        echo '<p class="article-cat">'.$category_display.'</p>';
                                                    endif;
                                                    ?>

                                                    <p class="podcast-text">Delicious. Podcast</p>

                                                    <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                    <?php if (has_excerpt()): ?>
                                                        <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 20, $more = '...'); ?>
                                                        <p><?php echo $excerpt_text; ?></p>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="card-footer">
                                                    <div class="article-stats nav-list">
                                                        <ul>
                                                            <li class="article-author"><?php the_author(); ?></li>
                                                            <li class="article-date"><?php the_date(); ?></li>
                                                            <li class="article-comments">
                                                                <span class="icon-inline icon-comments-green"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" /></span>
                                                                <span><?php echo $comments_count; ?></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php
                                    endwhile;

                                    wp_reset_postdata();
                                else:
                                ?>
                                    <p><?php esc_html_e('Sorry, there are currently no wine articles.'); ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php // COOK'S DICTIONARY ?>
            <section>
                <div class="content-block double-margin">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="intro-text text-standard">
                                <h2><?php echo get_cat_name($category_id = 1040); ?></h2>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div id="dictionary-tabs">
                                <ul class="tabs">
                                    <li><a href="#a"><?php echo get_cat_name($category_id = 1111); ?></a></li>
                                    <li><a href="#b"><?php echo get_cat_name($category_id = 1112); ?></a></li>
                                    <li><a href="#c"><?php echo get_cat_name($category_id = 1113); ?></a></li>
                                    <li><a href="#d"><?php echo get_cat_name($category_id = 1114); ?></a></li>
                                    <li><a href="#e"><?php echo get_cat_name($category_id = 1115); ?></a></li>
                                    <li><a href="#f"><?php echo get_cat_name($category_id = 1116); ?></a></li>
                                    <li><a href="#g"><?php echo get_cat_name($category_id = 1117); ?></a></li>
                                    <li><a href="#h"><?php echo get_cat_name($category_id = 1118); ?></a></li>
                                    <li><a href="#j"><?php echo get_cat_name($category_id = 1119); ?></a></li>
                                    <li><a href="#k"><?php echo get_cat_name($category_id = 1120); ?></a></li>
                                    <li><a href="#l"><?php echo get_cat_name($category_id = 1121); ?></a></li>
                                    <li><a href="#m"><?php echo get_cat_name($category_id = 1122); ?></a></li>
                                    <li><a href="#n"><?php echo get_cat_name($category_id = 1123); ?></a></li>
                                    <li><a href="#o"><?php echo get_cat_name($category_id = 1124); ?></a></li>
                                    <li><a href="#p"><?php echo get_cat_name($category_id = 1125); ?></a></li>
                                    <li><a href="#q"><?php echo get_cat_name($category_id = 1126); ?></a></li>
                                    <li><a href="#r"><?php echo get_cat_name($category_id = 1127); ?></a></li>
                                    <li><a href="#s"><?php echo get_cat_name($category_id = 1128); ?></a></li>
                                    <li><a href="#t"><?php echo get_cat_name($category_id = 1129); ?></a></li>
                                    <li><a href="#v"><?php echo get_cat_name($category_id = 1130); ?></a></li>
                                    <li><a href="#w"><?php echo get_cat_name($category_id = 1131); ?></a></li>
                                </ul>

                                <div id="a" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1111); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1111
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                            ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="b" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1112); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1112
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="c" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1113); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1113
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="d" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1114); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1114
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="e" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1115); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1115
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="f" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1116); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1116
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="g" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1117); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1117
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="h" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1118); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1118
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="j" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1119); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1119
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="k" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1120); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1120
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="l" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1121); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1121
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="m" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1122); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1122
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="n" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1123); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1123
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="o" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1124); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1124
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="p" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1125); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1125
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="q" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1126); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1126
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="r" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1127); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1127
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="s" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1128); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1128
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="t" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1129); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1129
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="v" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1130); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1130
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>

                                <div id="w" class="dictionary-category">
                                    <div class="letter">
                                        <span><?php echo get_cat_name($category_id = 1131); ?></span>
                                    </div>

                                    <div class="post-listing">
                                        <ul>
                                            <?php
                                            $args = array(
                                                'post_type' => 'post',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'orderby' => 'title',
                                                'order' => 'ASC',
                                                'cat' => 1131
                                            );
                                            $the_query = new WP_Query($args);

                                            if ($the_query->have_posts()):
                                                while ($the_query->have_posts()) : $the_query->the_post();
                                                    ?>
                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            else:
                                                ?>
                                                <li><?php esc_html_e('Sorry, there are currently no articles for this letter.'); ?></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

<?php get_footer();
