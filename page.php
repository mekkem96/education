<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section>
                <div class="row no-gutters">
                    <?php if (has_post_thumbnail()): ?>
                        <div class="col-sm-12 col-md-6">
                            <div class="content-block col-gutter">
                                <div class="copy">
                                    <h1><?php the_title(); ?></h1>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12 col-md-6">
                            <div class="article-image full-width">
                                <?php echo get_the_post_thumbnail(); ?>

                                <?php
                                $caption = get_post(get_post_thumbnail_id())->post_excerpt;

                                if(!empty($caption)){//If caption is not empty show the div
                                    echo '<div class="media-caption">'.$caption.'</div>';
                                }
                                ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="col-sm-12 col-md-8 col-lg-9">
                            <div class="content-block col-gutter">
                                <div class="copy">
                                    <h1><?php the_title(); ?></h1>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!has_post_thumbnail()): ?>
                        <?php // SIDEBAR ?>
                        <div class="col-sm-12 col-md-4 col-lg-3 hide-tablet">
                            <aside class="sidebar">
                                <div class="row no-gutters row-deep">
                                    <div class="col-12 align-self-start">
                                        <!-- Advertisement: Desktop > 1024 -->
                                        <div class="hide-tablet">
                                            <div class="advert-content content-block double-margin">
                                                <!-- Async AdSlot 3 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[300,600],[300,250]] -->
                                                <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[2]]) -->
                                                <div id='div-gpt-ad-1420540-3'>
                                                    <script>
                                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-3'); });
                                                    </script>
                                                </div>
                                                <!-- End AdSlot 3 -->
                                            </div>
                                        </div>
                                        <!-- /Advertisement: Desktop > 1024 -->
                                    </div>
                                </div>
                            </aside>
                        </div>
                    <?php endif; ?>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

<?php get_footer();
