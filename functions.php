<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function twentyseventeen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'twentyseventeen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentyseventeen' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'twentyseventeen-featured-image', 2000, 1200, true );

	add_image_size( 'twentyseventeen-thumbnail-avatar', 100, 100, true );

	add_image_size( 'profile', 25, 25, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'main-menu'    => __( 'Main Menu', 'twentyseventeen' ),
		'footer-links'    => __( 'Footer Links', 'twentyseventeen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', twentyseventeen_fonts_url() ) );
}
add_action( 'after_setup_theme', 'twentyseventeen_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function twentyseventeen_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( twentyseventeen_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'main-sidebar' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Twenty Seventeen content width of the theme.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'twentyseventeen_content_width', $content_width );
}
add_action( 'template_redirect', 'twentyseventeen_content_width', 0 );

/**
 * Register custom fonts.
 */
function twentyseventeen_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$libre_franklin = _x( 'on', 'Libre Franklin font: on or off', 'twentyseventeen' );

	if ( 'off' !== $libre_franklin ) {
		$font_families = array();

		$font_families[] = 'Libre Franklin:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentyseventeen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentyseventeen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentyseventeen_widgets_init() {
	register_sidebar( array(
		'name'          => __('Main Sidebar', 'twentyseventeen'),
		'id'            => 'main-sidebar',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));
}
add_action( 'widgets_init', 'twentyseventeen_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function twentyseventeen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'twentyseventeen_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function twentyseventeen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyseventeen_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function twentyseventeen_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'twentyseventeen_pingback_header' );

/**
 * Display custom color CSS.
 */
function twentyseventeen_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo twentyseventeen_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'twentyseventeen_colors_css_wrap' );

/**
 * Enqueue scripts and styles.
 */
function delicious_scripts() {
    // Wordpress required stylesheet.
    wp_enqueue_style('wp-default', get_stylesheet_uri());

    // Styles
    wp_enqueue_style('normalize', get_stylesheet_directory_uri().'/assets/css/normalize.min.css');
    wp_enqueue_style('bootstrap-grid', get_stylesheet_directory_uri().'/assets/css/bootstrap-grid.css', '4.1.1');
    wp_enqueue_style('bootstrap-components', get_stylesheet_directory_uri().'/assets/css/bootstrap-components.css', '4.1.1');
    wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', '5.2.0');
    wp_enqueue_style('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', '1.8.1');
    //wp_enqueue_style('mobile-menu', get_stylesheet_directory_uri().'/assets/css/jquery-simple-mobilemenu.css', '');
    wp_enqueue_style('formstyler-css', get_stylesheet_directory_uri().'/assets/css/jquery.formstyler.css', '');
    wp_enqueue_style('remodal-css', get_stylesheet_directory_uri().'/assets/css/remodal.css', '');
    wp_enqueue_style('rstyler-css', get_stylesheet_directory_uri().'/rfiles/css/style.css', '');
    wp_enqueue_style('add', get_stylesheet_directory_uri().'/assets/css/add.css', '');
    wp_enqueue_style('main', get_stylesheet_directory_uri().'/assets/css/main.css', '1.0.0');

    // Scripts
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', null, null, true);
    wp_enqueue_script('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', array('jquery'), null, true);
    wp_enqueue_script('modernizr', get_stylesheet_directory_uri().'/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js', null, null, true);
    wp_enqueue_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js', null, null, true);
    wp_enqueue_script('scrollto', get_stylesheet_directory_uri().'/assets/js/jquery.scrollTo.js', array('jquery'), '2.1.2');
    wp_enqueue_script('slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'), '1.8.1');
    wp_enqueue_script('sticky-ads', get_stylesheet_directory_uri().'/assets/js/jquery.stickyAds.js', array('jquery'), '1.0.0');
    wp_enqueue_script('hoverintent', get_stylesheet_directory_uri().'/assets/js/jquery.hoverIntent.min.js', array('jquery'), '1.9.0');
    //wp_enqueue_script('mobile-menu', get_stylesheet_directory_uri().'/assets/js/jquery-simple-mobilemenu.min.js', array('jquery'), '');
    wp_enqueue_script('formstyler-js', get_stylesheet_directory_uri().'/assets/js/jquery.formstyler.min.js', array('jquery'), '');
    wp_enqueue_script('remodal-js', get_stylesheet_directory_uri().'/assets/js/remodal.min.js', array('jquery'), '1.0.0');
    wp_enqueue_script('rfile-isotope', get_stylesheet_directory_uri().'/rfiles/isotope.pkgd.min.js', array('jquery'), '1.0.0');
    //wp_enqueue_script('rfile-scripts', get_stylesheet_directory_uri().'/rfiles/rscripts.js', array('jquery'), '1.0.0');
    wp_enqueue_script('rfile-ajax-search', get_stylesheet_directory_uri().'/rfiles/ajax-search.js', array('jquery'), '1.0.0');
    wp_enqueue_script('add-scripts', get_stylesheet_directory_uri().'/assets/js/add.js', array('jquery'), '1.0.0');
    wp_enqueue_script('scripts', get_stylesheet_directory_uri().'/assets/js/scripts.min.js', array('jquery'), '1.0.0');
    wp_localize_script( 'rfile-scripts', "search_obj", array(
        "ajax_url"      =>  admin_url( "admin-ajax.php" )
    ));
    wp_localize_script( 'custom-js', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
}
add_action('wp_enqueue_scripts', 'delicious_scripts');

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentyseventeen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentyseventeen_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function twentyseventeen_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentyseventeen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentyseventeen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function twentyseventeen_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'twentyseventeen_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Seventeen 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentyseventeen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentyseventeen_widget_tag_cloud_args' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );


/* ALLOW SVG UPLOADS TO MEDIA LIBRARY */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/* ACF: GLOBAL SITE OPTIONS */
if(function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' 	=> 'Site Options',
        'menu_title'	=> 'Site Options',
        'menu_slug' 	=> 'site-options',
    ));
}


add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
function my_wp_nav_menu_objects($items, $args) {
    // loop
    foreach($items as &$item) {
        // vars
        $image = get_field('image', $item);


        // append icon
        if($image) {
            $item->title .= '<div class="menu-item-image"><img class="svg" src="'.$image.'" alt="Menu icon or image" /></div>';
        } else {
            $item->title .= '<div class="menu-item-image"><img src="/wp-content/uploads/2018/08/492097-1-eng-GB_frozen-berries-with-hot-white-chocolate-sauce.jpg" alt="Menu icon or image" /></div>';
        }
    }

    // return
    return $items;
}


/* GET CATEGORIES MARKED AS PRIMARY OF POSTS */
function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

function get_recipes_delete() {
    global $wpdb;

    $post_id = $_REQUEST['post_id'];
    $user_id = $_REQUEST['user_id'];
    $field = $_REQUEST['field'];
    echo $post_id;
    echo $user_id;
    $meta= get_the_author_meta( $field, $user_id);

    $posts[] = $post_id;
    $result = array_diff($meta , $posts);
    update_user_meta($user_id, $field, $result);

    // $meta = user_meta($user_id , 'recipes');
    //print_r($meta);
    // delete_user_meta( $user_id, 'recipes'  );


    die();

}
add_action( 'wp_ajax_get_recipes_delete', 'get_recipes_delete' );
add_action('wp_ajax_nopriv_get_recipes_delete', 'get_recipes_delete');


function get_recipes_save(){
    global $wpdb;

    $post_id = $_REQUEST['post_id'];
    $user_id = $_REQUEST['user_id'];
    $field = $_REQUEST['field'];
    echo $post_id;
    echo $user_id;
    if (!$_REQUEST['field']) {
        $field = 'recipes';
    }
    echo $field;
    $meta= get_the_author_meta( $field, $user_id);

    //$result = array_push($meta , $post_id);
    $count = 0;
    foreach ($meta as $key => $value) {
        if ($value == $post_id) {
            $count++;
        }
    }
    if ($meta && $count==0) {
        $meta[] = $post_id;
        update_user_meta($user_id, $field, $meta);
        echo "update";

    }elseif(!$meta){
        $meta[] = $post_id;
        add_user_meta ($user_id, $field, $meta);
        update_user_meta($user_id, $field, $meta);
        echo "add";
    }



    die();

}
add_action( 'wp_ajax_get_recipes_save', 'get_recipes_save' );
add_action('wp_ajax_nopriv_get_recipes_save', 'get_recipes_save');


function hide_admin_wpse_93843() {
    if (current_user_can('subscriber')) {
        add_filter('show_admin_bar','__return_false');
    }
}
add_action('wp_head','hide_admin_wpse_93843');


/* --- AJAX functionality for search --- */
add_action( 'wp_ajax_ajax_search_callback', 'ajax_search_callback' );
add_action( 'wp_ajax_nopriv_ajax_search_callback', 'ajax_search_callback' );
if( ! function_exists( 'ajax_search_callback' ) ) {
    function ajax_search_callback(){
        $query = $_POST['search_query'];

        $result = '';

        $searchQuery = new WP_Query(
            array(
                's' => $query,
                'post_type'         =>  'recipe',
                'order'             =>  'ASC',
                'posts_per_page'    =>  9
            ));

        if( $searchQuery->have_posts() ) :
            $i = 0;
            $result .= '<div class="row">';
            while( $searchQuery->have_posts() ) : $searchQuery->the_post();

                $result .= '<div class="col-md-4 col-sm-6 col-xs-12 search_item"><div>';
                $result .= '<div class="item_img" style="background: url(' . get_the_post_thumbnail_url() . ') no-repeat center center;background-size: cover;"></div>';
                $result .= '<div class="item_title"><h5><a href="'.get_permalink().'">' . get_the_title() . '</a></h5>';
                $result .= '<div class="item_attr row">';
//                        $result .= '<div class="col-sm-6 item_recipes">421 recipes</div>';
                $stars = (float) yasr_get_overall_rating(get_the_ID());
                $result .= '<div class="col-sm-6 item_stars"><span class="stars_active" style="width:'.($stars*18).'px;"></span><span class="stars_passive"></span></div>';
                $result .= '<div class="col-sm-6 item_score"><span class="icon-inline icon-effort-main">
                                        <img src="' . get_template_directory_uri() .'/assets/icons/effort-green.svg" alt="Effort icon" />';
                $result .= '</span>';
                if(get_field('difficulty') == 'Easy') :
                    $result .= '<span>Easy</span>';
                elseif (get_field('difficulty') == 'Medium') :
                    $result .= '<span>Medium</span>';
                elseif (get_field('difficulty') == 'Challenge'):
                    $result .= '<span>A challenge<span>';
                endif;
                $result .= '</div>';
                $result .= '</div>';
                $result .= '</div>';
                $result .= '</div></div>';

                $i++;
            endwhile;
            $result .= '</div>';
        else :
            $result = false;
        endif;
        $searchQuery->reset_postdata();

        /* filter */
        $result = str_replace(str_replace('http://', '', esc_url(home_url('/'))), 'deliciousnew.wpengine.com', $result);

        echo $result;
        die();
    }
}