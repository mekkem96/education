<?php
/**
 * The template for displaying author archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section>
                <div class="article-intro-wrapper content-block">
                    <div class="row">
                        <?php $author_id = $post->post_author; ?>
                        <div class="col-sm-12 col-md-6">
                            <div class="article-author">
                                <div class="author-details">
                                    <div class="author-image full-width">
                                        <?php echo get_avatar($author_id); ?>
                                        <h1><?php the_author_meta('display_name', $author_id); ?></h1>
                                    </div>
                                    <p><?php the_author_meta('description', $author_id); ?></p>
                                    <div class="social-links">
                                        <div class="social-sharing-links">
                                            <div class="row no-gutters">
                                                <ul>
                                                    <?php if (get_field('facebook', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('facebook', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                    <?php endif; ?>
                                                    <?php if (get_field('instagram', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('instagram', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                    <?php endif; ?>
                                                    <?php if (get_field('twitter', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('twitter', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                    <?php endif; ?>
                                                    <?php if (get_field('pinterest', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('pinterest', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                                                    <?php endif; ?>
                                                    <?php if (get_field('youtube', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('youtube', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row no-gutters">
                    <div class="col content-block">
                        <div class="filters">
                            <?php $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy')); ?>
                            <p class="count"><?php echo $term->count; ?> articles</p>

                            <div class="layout-options">
                                <span class="double">Double</span>
                                <span class="single">Single</span>
                            </div>
                        </div>

                        <div class="recipe-list">
                            <?php
                            if (have_posts()): ?>
                                <?php $count = 0; ?>
                                <?php while (have_posts()) : the_post(); ?>
                                    <?php $count++; ?>
                                    <?php if ($count === 9) : ?>
                                        <section style="float: left;width: 100%;">
                                            <div class="row no-gutters">
                                                <!-- Advertisement -->
                                                <div class="col-12">
                                                    <div class="advert-content content-block double-margin">
                                                        <div class="row justify-content-center no-gutters">
                                                            <div class="col">
                                                                <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                                                <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                                                <div id='div-gpt-ad-1420540-2'>
                                                                  <script>
                                                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                                                  </script>
                                                                </div>
                                                                <!-- End AdSlot 2 -->
                                                            </div>
                                                            <p class="banner-ad"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    <?php else : ?>
                                        <a href="<?php the_permalink(); ?>" <?php post_class('recipe-card card-bg card-utilities'); ?>>
                                            <div class="card-img-wrapper">
                                                <div class="cta-icon icon-only cta-icon-not-saved-white">
                                                    <span>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save recipe icon" />
                                                    </span>
                                                </div>

                                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top">
                                            </div>

                                            <div class="card-body">
                                                <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                <?php $teaser_text = wp_trim_words(get_field('teaser_large'), $num_words = 15, $more = '...'); ?>
                                                <p><?php echo $teaser_text; ?></p>
                                            </div>

                                            <div class="card-footer">
                                                <div class="recipe-stats nav-list">
                                                    <ul>
                                                        <li class="recipe-rating">
                                                            <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                                                        </li>
                                                        <li class="recipe-skill">
                                                                <span class="icon-inline icon-effort-main">
                                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                                                                </span>
                                                            <?php if(get_field('difficulty') == 'Easy'): ?>
                                                                <span>Easy</span>
                                                            <?php elseif (get_field('difficulty') == 'Medium'): ?>
                                                                <span>Medium</span>
                                                            <?php elseif (get_field('difficulty') == 'Challenge'): ?>
                                                                <span>A challenge</span>
                                                            <?php endif; ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </a>
                                    <?php endif; ?>
                                <?php
                                endwhile;
                            else:
                                echo '<p>This author currently has no articles</p>';
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </section>

            <section id="utilities" class="hide-tablet content-block">
                <div class="article-utilities">
                    <div class="row no-gutters">
                        <div class="col">
                            <a href="#" title="Save article" class="bttn bttn-icon-save">
                                <span><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save icon" /></span>
                                Save this collection
                            </a>
                        </div>

                        <div class="col-auto">
                            <div class="social-sharing-links">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <div class="sharing-title text-standard">
                                            <h4 class="heading-std-text">Share this collection</h4>
                                        </div>
                                    </div>

                                    <div class="col-auto">
                                        <div class="sharing-links">
                                            <?php echo do_shortcode('[ssba-buttons]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <!-- Advertisement -->
                    <div class="col-12">
                        <div class="advert-content content-block double-margin">
                            <div class="row justify-content-center no-gutters">
                                <div class="col">
                                    <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                    <div id='div-gpt-ad-7890863-2'>
                                      <script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-7890863-2'); });</script>
                                    </div>
                                    <!-- End AdSlot 2 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

<?php get_footer();