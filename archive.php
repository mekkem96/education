<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
$cat = get_queried_object();
$authorid=get_current_user_id();
$term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section>
                <div class="row no-gutters">
                    <div class="col-sm-12 col-md-4">
                        <div class="recipe-heading text-standard">
                            <div class="recipe-title">
                                <h1><?php single_cat_title(); ?></h1>
                                <?php the_archive_description(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col order-md-last">
                        <?php if(get_field('sponsor_name', $term)): ?>
                            <div class="sponsor">
                                <div class="sponsor-text">Sponsored by<br><?php the_field('sponsor_name', $term); ?></div>

                                <div class="sponsor-logo">
                                    <?php
                                    $sponsor_logo = get_field('sponsor_logo', $term);

                                    if(!empty($sponsor_logo)): ?>
                                        <img src="<?php echo $sponsor_logo['url']; ?>" alt="<?php echo $sponsor_logo['alt']; ?>" class="img-responsive" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <div class="col content-block">
                        <div class="filters">
                            <p class="count"><?php echo $term->count; ?> recipes</p>

                            <div class="layout-options">
                                <span class="double"></span>
                                <span class="single"></span>
                            </div>
                        </div>

                        <div class="recipe-list">
                            <?php
                            if (have_posts()): ?>
                                <?php $count = 0; ?>
                                <?php while (have_posts()) : the_post(); ?>
                                    <a href="<?php the_permalink(); ?>" <?php post_class('recipe-card card-bg card-utilities'); ?>>
                                        <div class="card-img-wrapper">
                                            <div class="cta-icon icon-only cta-icon-not-saved-white">
                                                <span>
                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save recipe icon" />
                                                </span>
                                            </div>

                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                            <?php if (get_field('sponsor_name')): ?>
                                                <p>Sponsored by <?php the_field('sponsor_name'); ?></p>
                                            <?php endif; ?>
                                        </div>

                                            <div class="card-body">
                                                <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                <?php $teaser_text = wp_trim_words(get_field('teaser_large'), $num_words = 15, $more = '...'); ?>
                                                <p><?php echo $teaser_text; ?></p>
                                            </div>

                                        <div class="card-footer">
                                            <div class="recipe-stats nav-list">
                                                <ul>
                                                    <li class="recipe-rating">
                                                        <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                                                    </li>
                                                    <li class="recipe-skill">
                                                            <span class="icon-inline icon-effort-main">
                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                                                            </span>
                                                        <?php if(get_field('difficulty') == 'Easy'): ?>
                                                            <span>Easy</span>
                                                        <?php elseif (get_field('difficulty') == 'Medium'): ?>
                                                            <span>Medium</span>
                                                        <?php elseif (get_field('difficulty') == 'Challenge'): ?>
                                                            <span>A challenge</span>
                                                        <?php endif; ?>
                                                    </li>
                                                    <li class="recipe-comments">
                                                        <span class="icon-inline icon-comments-green">
                                                            <img src="http://deliciousnew.wpengine.com/wp-content/themes/delicious/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                        </span>

                                                        <?php
                                                        $args = array(
                                                            'post_id' => get_the_ID(), // use post_id, not post_ID
                                                            'count' => true //return only the count
                                                        );
                                                        $comments_count = get_comments($args);
                                                        ?>
                                                        <span><?php echo $comments_count; ?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </a>
                                    <?php $count++; ?>
                                    <?php if ($count === 8) : ?>
                                        <section style="float: left;width: 100%;">
                                            <div class="row no-gutters">
                                                <!-- Advertisement -->
                                                <div class="col-12">
                                                    <div class="advert-content content-block double-margin">
                                                        <div class="row justify-content-center no-gutters">
                                                            <div class="col">
                                                                <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                                                <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                                                <div id='div-gpt-ad-1420540-2'>
                                                                  <script>
                                                                    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                                                  </script>
                                                                </div>
                                                                <!-- End AdSlot 2 -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    <?php endif; ?>
                                <?php
                                endwhile;
                            else:
                                echo '<p>There are currently no recipes for this collection.</p>';
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </section>

            <section id="utilities" class="hide-tablet content-block">
                <div class="article-utilities">
                    <div class="row no-gutters">
                        <div class="col">
                            <a href="javascript:void(0)" title="Save article" class="bttn bttn-icon-save btn-save-collection" post_id="<?php echo $cat->term_id;?>" user_id="<?php echo $authorid;?>">
                                <span><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save icon" /></span>
                                Save this collection
                            </a>
                        </div>

                        <div class="col-auto">
                            <div class="social-sharing-links">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <div class="sharing-title text-standard">
                                            <h4 class="heading-std-text">Share this collection</h4>
                                        </div>
                                    </div>

                                    <div class="col-auto">
                                        <div class="sharing-links">
                                            <?php echo do_shortcode('[ssba-buttons]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <!-- Advertisement -->
                    <div class="col-12">
                        <div class="advert-content content-block double-margin">
                            <div class="row justify-content-center no-gutters">
                                <div class="col">
                                    <!-- Async AdSlot 4 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[3]]) -->
                                    <div id='div-gpt-ad-1420540-4'>
                                      <script>
                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-4'); });
                                      </script>
                                    </div>
                                    <!-- End AdSlot 4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                //    event.preventDefault();
                var page = 2;

                $('.cta-icon').click(function(e){
                    var post_id = $(this).attr('post_id');
                    var user_id = $(this).attr('user_id');
                    var field = 'recipes';
                    $(this).parent().parent().addClass('recipe-card--loading');
                    /*console.log(post_id);
                    console.log(user_id);*/
                    $.ajax({
                        url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                        data: {
                            'action': 'get_recipes_save',
                            'post_id' : post_id,
                            'user_id' : user_id,
                            'field' : field
                        },
                        success:function(data) {
                            /*console.log(data);
                            console.log(post_id);*/
                            /*$('[id=' + post_id + ']').css('display','none');*/
                            $('div.post-' + post_id)
                                .removeClass('recipe-card--loading')
                                .find('.cta-icon img').attr('src', '<?= esc_url(home_url('/wp-content/themes/delicious/assets/icons/saved-white.svg')) ?>');
                        }
                    });
                });

                $('.btn-save-collection').click(function() {
                    var post_id = $(this).attr('post_id');
                    var user_id = $(this).attr('user_id');
                    /*console.log(post_id);
                    console.log(user_id);*/
                    $(this).parent().parent().addClass('recipe-card--loading');
                    $.ajax({
                        url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                        data: {
                            'action': 'get_recipes_save',
                            'post_id' : post_id,
                            'user_id' : user_id,
                            'field' : 'collection'
                        },
                        success:function(data) {
                            /*console.log(data);
                            console.log(post_id);*/
                            $('a.btn-save-collection img').attr('src', '<?= esc_url(home_url('/wp-content/themes/delicious/assets/icons/saved-white.svg')); ?>');
                        }
                    });
                });


            })
        })(jQuery);
    </script>

<?php get_footer();