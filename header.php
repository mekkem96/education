<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <!-- Start GPT Async Tag -->
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
        var gptadslots = [];
        var googletag = googletag || {cmd:[]};
    </script>

    <script>
        googletag.cmd.push(function() {
            var mapping1 = googletag.sizeMapping()
                .addSize([1000, 10], [[970, 250], [728, 90]])
                .addSize([740, 10], [[728, 90]])
                .addSize([0, 0], [[320, 50], [300, 50]])
                .build();

            var mapping2 = googletag.sizeMapping()
                .addSize([1000, 10], [[970, 250], [728, 90]])
                .addSize([740, 10], [[728, 90]])
                .addSize([0, 0], [[300, 250]])
                .build();

            var mapping3 = googletag.sizeMapping()
                .addSize([1000, 100], [[300, 600], [300, 250]])
                .addSize([740, 10], [[300, 600], [300, 250]])
                .addSize([0, 0], [[300, 250]])
                .build();

            //Adslot 1 declaration
            gptadslots.push(googletag.defineSlot('/176986657/deliciousmagazine.co.uk/home', [[970,250]], 'div-gpt-ad-1420540-1')
                             .setTargeting('pos', ['1', 'leader1', 'top'])
                             .defineSizeMapping(mapping1)
                             .addService(googletag.pubads()));

            //Adslot 2 declaration
            gptadslots.push(googletag.defineSlot('/176986657/deliciousmagazine.co.uk/home', [[970,250]], 'div-gpt-ad-1420540-2')
                             .setTargeting('pos', ['2', 'leader2'])
                             .defineSizeMapping(mapping2)
                             .addService(googletag.pubads()));

            //Adslot 3 declaration
            gptadslots.push(googletag.defineSlot('/176986657/deliciousmagazine.co.uk/home', [[300,600],[300,250]], 'div-gpt-ad-1420540-3')
                             .defineSizeMapping(mapping3)
                             .addService(googletag.pubads()));

            //Adslot 4 declaration
            gptadslots.push(googletag.defineSlot('/176986657/deliciousmagazine.co.uk/home', [[970,250]], 'div-gpt-ad-1420540-4')
                             .setTargeting('pos', ['3', 'leader3'])
                             .defineSizeMapping(mapping2)
                             .addService(googletag.pubads()));

            //Adslot 5 declaration - Out of Page
            gptadslots.push(googletag.defineOutOfPageSlot('/176986657/deliciousmagazine.co.uk/home', 'div-gpt-ad-1420540-5')
                             .addService(googletag.pubads()));

            googletag.pubads().enableSingleRequest();
            googletag.pubads().setTargeting('env', ['uat']);
            googletag.pubads().setForceSafeFrame(false);
            googletag.enableServices();
        });
    </script>
    <!-- End GPT Async Tag -->
</head>

<body <?php body_class(); ?>>
    <?php // Advertisement - Desktop ?>
    <div class="content-wrapper">
        <header class="sticky-header">
            <div class="top-banner-ad-desktop top-banner-ad non-stick">
                <div class="advert-content">
                    <div class="content-wrapper">
                        <div class="row justify-content-center no-gutters">
                            <!-- Async AdSlot 1 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                            <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[0]]) -->
                            <div id='div-gpt-ad-1420540-1'>
                              <script>
                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-1'); });
                              </script>
                            </div>
                            <!-- End AdSlot 1 -->
                        </div>
                    </div>
                </div>
            </div>

            <?php // Header - Desktop ?>
            <div id="header-desktop" class="hide-mobile content-wrapper">
                <header id="header-content" class="below-ad">
                    <div class="header-wrapper">
                        <div class="header-elements">
                            <div class="row">
                                <div class="col">
                                    <div class="links-functional links-user">
                                        <nav class="nav-list">
                                            <ul>
                                                <li><a href="https://www.subscription.co.uk/EyeToEye/Delicious/Store/Subscription/DLC/IDLC0818" target="_blank">Subscribe to magazine</a></li>
                                                <li><a href="#">Shop at the Deli</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>

                                <div class="col">
                                    <a href="/" class="logo"><span class="sr-only">delicious.</span></a>
                                    <a href="/" class="logo-small"><span class="sr-only">delicious.</span></a>
                                </div>

                                <div class="col">
                                    <div class="links-functional links-account">
                                        <nav class="nav-list">
                                            <ul>
                                                <?php if(is_user_logged_in()):?>
                                                    <li class="">
                                                        <a href="/account" class="t_inaccount active">
                                                            <?php
                                                            $authorid =get_current_user_id( );
                                                            $authorimage = get_field('image', 'user_'.$authorid.'');
                                                            if($authorimage):?>
                                                                <img src="<?php echo $authorimage['sizes']['profile'];?>" alt="">
                                                            <?php else: $id_image = get_the_author_meta('image',$authorid);
                                                                $id_image2 =  wp_get_attachment_image($id_image); ?>
                                                                <img src="<?php echo $id_image2; ?>" alt="">
                                                            <?php endif; ?>
                                                            Hi, <?php echo get_the_author_meta('first_name',$authorid); ?>
                                                        </a>
                                                    </li>
                                                <?php else:?>
                                                    <li>
                                                        <a href="<?php echo get_permalink('20281'); ?>" class="<?php if( get_the_ID() == '20281'){echo 'active';} ?>">Log in</a>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo get_permalink('20282'); ?>" class="<?php if( get_the_ID() == '20282'){echo 'active';} ?>">Register</a>
                                                    </li>
                                                <?php endif;?>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php // Nav: Desktop > 1024 ?>
                        <div class="content-block">
                            <div class="nav-main">
                                <?php get_template_part('template-parts/navigation/navigation', 'main-menu'); ?>

                                <div class="cta-search" id="desctopsearch">
                                    <a href="#">
                                        Search
                                        <span class="icon-inline icon-search">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/search-green.svg" alt="Search icon" />
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if (is_singular('recipes')): ?>
                    <div class="nav-page">
                        <div class="row no-gutters">
                            <div class="col">
                                <nav class="anchors">
                                    <ul>
                                        <li class="ingredients"><a data-anchor="ingredients">Ingredients</a></li>
                                        <li class="method"><a data-anchor="method">Method</a></li>
                                        <li class="tips"><a data-anchor="tips">delicious. Tips</a></li>
                                        <li class="related"><a data-anchor="nutrition">Related Collections</a></li>
                                        <li class="rate"><a data-anchor="nutrition">Rate and Reviews</a></li>
                                    </ul>
                                </nav>
                            </div>

                            <div class="nav-page-utilities">
                                <ul>
                                    <li>
                                        <a href="#" title="Save recipe" class="bttn-icon-save">
                                            <span class="icon-inline icon-not-saved-white"></span>
                                            Save recipe
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Print recipe" class="bttn-icon-print">
                                            <span class="icon-inline icon-print-white"></span>
                                            Print
                                        </a>
                                    </li>
                                </ul>

                                <div class="sharing-links">
                                    <?php echo do_shortcode('[ssba-buttons]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="t_searchplace t_searchplace_new_class t_searchplace_new_class2">
                        <div class="t_searchwrap">
                            <input type="text" placeholder="Type here to find recipes, collections, articles...">

                            <button class="t_search_desc">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arrow-right-black.svg" class="Arrow-black">
                            </button>
                        </div>
                    </div>
                </header>
            </div>

            <?php // HEADER MOBILE ?>
            <div id="header-mobile" class="visible-mobile content-wrapper">
                <header id="header-content-mobile" class="below-ad">
                    <div class="header-wrapper">
                        <div class="header-elements">
                            <div class="row justify-content-between no-gutters">
                                <div class="col-auto">
                                    <a href="/" class="logo"><span class="sr-only">delicious</span></a>
                                </div>

                                <div class="col-auto">
                                    <?php // Nav: Mobile < 1024 ?>
                                    <div class="nav-main">
                                        <div class="cta-search">
                                            <a href="#"></a>
                                        </div>

                                        <div class="nav-mobile-cta">
                                            <div class="burger-nav">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="fly-out-wrapper">
                            <div class="top-area">
                                <button type="button">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/close-icon.svg" alt="Close menu" />
                                </button>
                            </div>

                            <div class="account-section">
                                <?php if(is_user_logged_in()): ?>
                                    <div class="account-details">
                                        <a href="/account" class="t_inaccount active">
                                            <?php if (get_field('profile_picture', 'user_'.$author_id)): ?>
                                                <img src="<?php the_field('profile_picture', 'user_'.$author_id); ?>">
                                            <?php else: ?>
                                                <?php echo get_avatar($author_id); ?>
                                            <?php endif; ?>

                                            <p>Hi, <?php echo get_the_author_meta('first_name',$authorid); ?></p>
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <div class="account-buttons">
                                        <a href="<?php echo get_permalink('20281'); ?>" class="<?php if(get_the_ID() == '20281'){echo 'active';} ?>"><span>Log in</span></a>
                                        <a href="<?php echo get_permalink('20282'); ?>" class="<?php if(get_the_ID() == '20282'){echo 'active';} ?>"><span>Register</span></a>
                                    </div>
                                <?php endif;?>
                            </div>

                            <div class="mobile-nav">
                                <?php get_template_part('template-parts/navigation/navigation', 'main-menu'); ?>
                            </div>

                            <div class="external-links">
                                <nav class="nav-list">
                                    <ul>
                                        <li>
                                            <a href="https://www.subscription.co.uk/EyeToEye/Delicious/Store/Subscription/DLC/IDLC0818" target="_blank">
                                                <span>Subscribe to magazine</span>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/external-link.svg" alt="External link" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span>Shop at the Deli</span>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/external-link.svg" alt="External link" />
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        </header>
    </div>