<?php
/**
 * Template Name: Register
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
$Enter = 0;
get_header();
?>

    <div class="t_header_placeholder"></div>
    <div class="content-wrapper">
        <div class="col-12 t_register_banner">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="t_simplepad">
                        <h1>Why Register?</h1>
                        <ul>
                            <li>Receive personalised emails</li>
                            <li>Save all your favourites</li>
                            <li>Rate recipes and articles</li>
                            <li>Enter competitions</li>
                            <li>Comment, ask question and share ideas</li>
                            <li>Get the latest updates via email</li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 t_whiteleft" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_register.png); background-size: cover;">

                </div>
            </div>
        </div>
    </div>
<?php

$account = 0;
$first = $_POST['first'] ? $_POST['first'] : '';
$last= $_POST['last'];
$email = $_POST['email'] ? $_POST['email'] : '' ;
$radio= $_POST['radio'];
$password= $_POST['password'];
$c_password= $_POST['c_password'];
$key= $_REQUEST['key'];
$message = "Hi, $first. Activation code from delicious.co.uk";
$headers = 'From: <noreply@'.str_replace( array('http://','https://'), '', esc_url(home_url('/'))).'>';

if ($_REQUEST['key']) {
    $activs = $_REQUEST['key'];
    $wpdb->get_results("UPDATE wp_users set user_key = 1 where user_activation_key  = '$activs' ");
    echo "<span>Actived</span>";
    $_REQUEST['key'] = '';
}

if($_POST['email'] ) { $email = $_POST['email']; $key = $wpdb->get_results("SELECT *from `wp_users` where `user_email` ='$email'  ");}


//if (isset($key[0]->ID)) {	echo "sdfssssssssssssssssss";	print_r($key);	# code...	}

$user_id = $key[0]->ID;
if(isset($key[0]->ID)){
    $account = 1;
}
$rand = mt_rand(0, 65);
$code = md5($rand . time());
$kod= esc_url(home_url('/register/?key='.$code));
//echo $key[0]->ID;
//echo $account;
//echo $kod;

if($_POST && $account ===0 && $_POST['email'] !== '' && $_POST['first']!=='' && $_POST['last'] !=='' &&!isset($key[0]->ID) && $password === $c_password && $_POST['password']!=='' && $_POST['c_password'] !=='' ){

    $send =  wp_mail($email, $message,$attachments = "Please, open this link to activate your profile $kod" ,$headers);
    update_user_meta($user_id , 'first',$first);
    update_user_meta($user_id , 'last',$last);
    if ($send) {$success = '<p><font color="green">Success! Check you email address.</font></p>';

    }
    else {$success = '<p><font color="red">Error: Mail sending failed.</font></p>';}
    echo $success;

    wp_create_user($first,$password, $email);
    header('Location: '.esc_url(home_url('/register/?user_email='.$email.'#confirm-modal')));

    //598740094483-h6jbmhlb6qk8jibaq0njpo51ctpec2ff.apps.googleusercontent.com
    $user_key = $wpdb->get_results("select *from wp_users where user_email  = '$email' ");
    $u_id = $user_key[0]->ID;
    //echo $u_id;
    $wpdb->get_results("UPDATE wp_users set user_activation_key  = '$code' where ID = '$u_id' or user_nicename ='$first' ");
    echo "<p><font color='green'>Your request Has Been Sent, Thank You.</font></p>";
    $_POST = array();

}else{
    if(isset($key[0]->ID)){ echo "<p><font color='red'>There is such a account</font></p>"; $Enter++; }
    if($_POST['email'] ==='' || $_POST['first'] ==='' || $_POST['last']  ==='' ) { $Enter++; echo "<p><font color='red'>Please fill out the form completely !</font></p>";}
    if((isset($_POST['password']) !== isset($_POST['c_password']) || !isset($_POST['password']) || !isset($_POST['c_password'])  )
        &&(isset($_POST['password']) === isset($_POST['c_password']) && !isset($_POST['password']) ==='') ) {
        echo "<p><font color='red'>Password incorrect or incomplete</font></p>";
        $Enter++;
    }
    ?>
    <div class="wrap nopad t_register_form">
        <form method = 'POST' action ='' name = 'register'>
            <div class="row">

                <div class="col-12 col-lg-6 ">
                    <div class="row t_form_styler left">
                        <div class="col-6 t_input_line">
                            <label for="name">First Name</label>
                            <input name = "first" type="text" id="name" value="<?php echo $_POST['first'];?>" >
                        </div>
                        <div class="col-6 t_input_line">
                            <label for="lastname">Last Name</label>
                            <input name = "last" type="text" id="lastname" value="<?php echo $_POST['last'];?>" >
                        </div>
                        <div class="col-12 t_input_line">
                            <label for="email">Email*</label>
                            <input name = "email" type="email" placeholder="Type your email here..." id="email" value="<?php echo $_POST['email'];?>" >
                        </div>
                        <div class="col-6 t_input_line">
                            <label for="password">Password*</label>
                            <input name = "password" type="password" id="password" value="<?php echo $_POST['password'];?>" >
                        </div>
                        <div class="col-6 t_input_line">
                            <label for="confirm">Confirm Password*</label>
                            <input name = "c_password"  type="password" id="confirm" value="<?php echo $_POST['c_password'];?>" >
                        </div>
                        <div class="col-12">
                            <div class="t_radio">
                                <input value= "I want to receive delicious newsletter" type="radio" name="radio" id="radio1" />
                                <label for="radio1">I want to receive delicious newsletter</label>
                            </div>
                            <div class="t_radio">
                                <input  value="I agree to our Terms & Conditions" type="radio" name="radio" id="radio2" />
                                <label for="radio2">I agree to our Terms & Conditions</label>
                                <small>Eye to Eye Media Ltd ("delicious. magazine") may contact you with details of its products, services and promotions. By registering to delicious. you agree to our terms & conditions and privacy policy.</small>
                            </div>
                            <div class="t_radio">
                                <input value="I want to receive offers from delicious partners" type="radio" name="radio" id="radio3" />
                                <label for="radio3">I want to receive offers from delicious partners</label>
                                <small>delicious. magazine would like you to receive offers and communications that may be of interest to you about other carefully selected companies products and services. In the case of email, these offers will only come to you from delicious. magazine.</small>
                            </div>
                        </div>
                        <div class="col-12 text-center">
                            <?php
                            if($Enter>0) $_POST['error'] = 1;
                            else{
                                $_POST['error'] = 0;
                            }
                            ?>
                            <input type="submit" id="reg1" value="Register Now" class="greenbutton"></a>
                        </div>

                    </div>
                    <form>

                </div>
                <a data-remodal-target="confirm-modal" href="#" id= "modal"></a>
                <div class="col-12 col-lg-6">
                    <div class="row t_form_styler text-center right">
                        <div class="col-12">
                            <label>Or register with your socials</label>
                            <div>
                                <a href="javascript:void(0)" class="t_facebook">Register with facebook</a>
                            </div>
                            <script type="text/javascript">
                                $( ".t_facebook" ).click(function() {
                                    $( ".bp_social_connect_facebook" ).trigger( "click" );
                                });
                            </script>

                            <div>
                                <a href="https://accounts.google.com/signin/oauth/oauthchooseaccount?client_id=598740094483-3qfi8gh2onl5e8euveqr3fsgei2c9k9k.apps.googleusercontent.com&as=KdYRVcj0F6XBI4qO0hl4BA&destination=http%3A%2F%2Fdelicious.1devserver.co.uk&approval_state=!ChRuLXhOYWNaY2h3UWRQXzYyWFhBShIfVTBRLVV3a1lEbnNTOERFdWhZOThQY18xQk01dlZoWQ%E2%88%99ANKMe1QAAAAAW4AJBfHYQzHis_a6ZwOlFrjWG1jJoo1a&oauthgdpr=1&xsrfsig=AHgIfE9rwsxToHli1rKnbxZCI0fYw1SIww&flowName=GeneralOAuthFlow" class="t_gmail">Register with gmail</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div><!-- .wrap -->

    <input type="hidden" id="bp_social_connect_security" name="bp_social_connect_security" value="984d7d2c0d" /><input type="hidden" name="_wp_http_referer" value="/wp-login.php?loggedout=true" />		<script type="text/javascript">
        var ajaxurl = '<?= esc_url(home_url('/')); ?>'
    </script>
    <style>
        .bp_social_connect_facebook , .bp_social_connect_google {
            display: none;
        }
        .t_facebook {
            cursor: pointer;
        }
    </style>
    <div class="bp_social_connect">		<div id="fb-root" class="bp_social_connect_fb"></div>
        <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : "269590670319265", // replace your app id here
                    status     : true,
                    cookie     : true,
                    xfbml      : true,
                    version    : 'v2.0'
                });
                FB.Event.subscribe('auth.authResponseChange', function(response){

                    if (response.status === 'connected'){
                        console.log('success');
                    }else if (response.status === 'not_authorized'){
                        console.log('failed');
                    } else{
                        console.log('unknown error');
                    }
                });
            };
            (function(d){
                var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement('script'); js.id = id; js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document));
            jQuery(document).ready(function($){
                $('.bp_social_connect_facebook').unbind('click');
                $('.bp_social_connect_facebook').on('click',function(){
                    var $this = $(this);
                    $this.addClass('loading');
                    var security = $('#bp_social_connect_security').val();
                    FB.login(function(response){
                        if (response.authResponse){

                            FB.api('/me', function(response) {
                                $.ajax({
                                    url: ajaxurl,
                                    data: 'action=bp_social_connect_facebook_login&id='+response.id+'&email='+response.email+'&first_name='+response.first_name+'&last_name='+response.last_name+'&gender='+response.gender+'&name='+response.name+'&link='+response.link+'&locale='+response.locale+'&security='+security,
                                    type: 'POST',
                                    dataType: 'JSON',
                                    success:function(data){
                                        $this.removeClass('loading');
                                        console.log(data);
                                        if (data.redirect_uri){
                                            if (data.redirect_uri =='refresh') {
                                                window.location.href =jQuery(location).attr('href');
                                            } else {
                                                window.location.href = data.redirect_uri;
                                            }
                                        }else{
                                            window.location.href = "<?= esc_url(home_url('/')); ?>";
                                        }
                                    },
                                    error: function(xhr, ajaxOptions, thrownError) {
                                        $this.removeClass('loading');
                                        window.location.href = "<?= esc_url(home_url('/')); ?>";
                                    }
                                });

                            });
                        }else{

                        }
                    }, {scope: 'email,user_likes', return_scopes: true});
                });
            });
        </script>
        <a class="bp_social_connect_facebook" href="javascript:void(0)">FACEBOOK</a><a class="bp_social_connect_google" href="https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=http%3A%2F%2Fdeliciousnew.wpengine.com&client_id=598740094483-7j4ad94valanrecfpvvbuljgojh8n09f.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&access_type=offline&approval_prompt=force">GOOGLE</a><style>.bp_social_connect { display: inline-block; width: 100%; }
            .bp_social_connect_facebook{background:#3b5998;}.bp_social_connect_google{background:#DD4B39 !important;}.bp_social_connect > a{text-align:center;float:left;padding:15px;border-radius:2px;color:#fff !important;width:200px;margin:0 5px;}.bp_social_connect > a:first-child{margin-left:0;}
            .bp_social_connect > a:before{float:left;font-size:16px;font-family:fontawesome;opacity:0.6;}.bp_social_connect_facebook:before{content:"\f09a";}.bp_social_connect_google:before{content:"\f0d5";}</style></div>	<script type="text/javascript">
        var elm_button_vars = { wrapper: '#recipe_list' };
    </script>






    <?php
} ?>
    <script type="text/javascript"> function onSignIn(googleUser) {


            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
        }

        (function($) {
            $(document).ready(function() {

                <?php
                if($_POST['error']===0 && isset($_POST['error'])&&$_POST['email']){?>
                $( "#modal" ).trigger( "click" );<?php

                }
                ?>


            })
        })(jQuery);
        //	event.preventDefault();
    </script>
<?php get_footer();