<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section id="article-overview">
                <?php if( get_field('sponsor_name') ): ?>
                <div class="article-header">
                    <div class="row no-gutters">
                        <div class="col-sm-12 col-md-12 order-md-last">
                            <div class="sponsor">
                                <div class="sponsor-text">Sponsored by<br><?php the_field('sponsor_name'); ?></div>

                                <div class="sponsor-logo">
                                    <?php
                                    $sponsor_logo = get_field('sponsor_logo');

                                    if(!empty($sponsor_logo)): ?>
                                        <img src="<?php echo $sponsor_logo['url']; ?>" alt="<?php echo $sponsor_logo['alt']; ?>" class="img-responsive" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <div class="article-intro-wrapper content-block">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="article-meta">
                                <?php
                                // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                $taxonomy = 'category';
                                $category = get_the_terms(get_the_ID(), $taxonomy);
                                $cat_link = true;

                                // If post has a category assigned.
                                if ($category):
                                    $category_display = '';
                                    $category_link = '';

                                    if (class_exists('WPSEO_Primary_Term')):
                                        // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                        $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_ID());
                                        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                        $term = get_term($wpseo_primary_term);

                                        if (is_wp_error($term)):
                                            // Default to first category (not Yoast) if an error is returned
                                            $category_display = $category[0]->name;
                                            $category_link = get_bloginfo('url').'/'.$term->slug;
                                        else:
                                            // Yoast Primary category
                                            $category_display = $term->name;
                                            $category_link = get_term_link($term->term_id);
                                        endif;
                                    else:
                                        // Default, display the first category in WP's list of assigned categories
                                        $category_display = $category[0]->name;
                                        $category_link = get_term_link($category[0]->term_id);
                                    endif;

                                    // Display category
                                    if (!empty($category_display)):
                                        if ($cat_link == true && !empty($category_link)):
                                            echo '<a href="'.$category_link.'">'.$category_display.'</a>';
                                        else:
                                            echo '<span class="post-category">'.$category_display.'</span>';
                                        endif;
                                    endif;
                                endif;

                                $args = array(
                                    'post_id' => get_the_ID(), // use post_id, not post_ID
                                    'count' => true //return only the count
                                );
                                $comments_count = get_comments($args);
                                ?>

                                <ul>
                                    <li class="article-date"><?php the_date(); ?></li>
                                    <li class="article-comments">
                                        <span class="icon-inline icon-comments-green">
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                        </span>
                                        <span><?php echo $comments_count; ?></span>
                                    </li>
                                </ul>
                            </div>

                            <div class="article-copy">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>

                            <div class="article-author">
                                <?php $author_id = $post->post_author; ?>
                                <div class="author-photo">
                                    <?php echo get_avatar($author_id); ?>
                                    <p><?php the_author_meta('display_name', $author_id); ?></p>
                                </div>

                                <div class="author-details">
                                    <p><?php the_author_meta('description', $author_id); ?></p>

                                    <div class="social-links">
                                        <div class="social-sharing-links">
                                            <div class="row no-gutters">
                                                <ul>
                                                    <?php if (get_field('facebook', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('facebook', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                    <?php endif; ?>
                                                    <?php if (get_field('instagram', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('instagram', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                    <?php endif; ?>
                                                    <?php if (get_field('twitter', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('twitter', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                    <?php endif; ?>
                                                    <?php if (get_field('pinterest', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('pinterest', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                                                    <?php endif; ?>
                                                    <?php if (get_field('youtube', 'user_'.$author_id)): ?>
                                                        <li><a href="<?php the_field('youtube', 'user_'.$author_id); ?>" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="author-link">Find more</a>
                                </div>
                                <div class="visible-tablet">
                                    <div class="row no-gutters">
                                        <!-- Advertisement -->
                                        <div class="col-12">
                                            <div class="advert-content content-block double-margin">
                                                <div class="row justify-content-center no-gutters">
                                                    <div class="col">
                                                        <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                                        <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                                        <div id='div-gpt-ad-1420540-2'>
                                                          <script>
                                                            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                                          </script>
                                                        </div>
                                                        <!-- End AdSlot 2 -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="article-image full-width">
                                <?php echo get_the_post_thumbnail(); ?>

                                <?php
                                $caption = get_post(get_post_thumbnail_id())->post_excerpt;

                                if(!empty($caption)){//If caption is not empty show the div
                                    echo '<div class="media-caption">'.$caption.'</div>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="content-block double-margin">
                <div class="article-details-wrapper">
                    <div class="row no-gutters">
                        <div class="col-sm-12 col-md-9">
                            <div class="copy">
                                <?php get_template_part('template-parts/page/content', 'layouts'); ?>
                            </div>
                        </div>

                        <?php // SIDEBAR ?>
                        <div class="sidebar col-sm-12 col-md-3 hide-tablet">
                            <div class="side-top-ad">
                                <aside class="sidebar ad">
                                    <div class="row no-gutters row-deep">
                                        <div class="col-12 align-self-start">
                                            <!-- Advertisement: Desktop > 1024 -->
                                            <div class="hide-tablet">
                                                <div class="advert-content content-block double-margin">
                                                    <!-- Async AdSlot 3 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[300,600],[300,250]] -->
                                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[2]]) -->
                                                    <div id='div-gpt-ad-1420540-3'>
                                                      <script>
                                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-3'); });
                                                      </script>
                                                    </div>
                                                    <!-- End AdSlot 3 -->
                                                </div>
                                            </div>
                                            <!-- /Advertisement: Desktop > 1024 -->
                                        </div>
                                    </div>
                                </aside>
                            </div>

                            <?php
                            $related_articles = get_field('articles');

                            if($related_articles):
                            ?>
                                <div class="col-12 align-self-end">
                                    <div class="related-articles content-block">
                                        <div class="intro-text text-standard">
                                            <h2>Read more</h2>
                                        </div>

                                        <div class="cards card-stack">
                                            <?php
                                            foreach($related_articles as $post): // variable must be called $post (IMPORTANT)
                                                setup_postdata($post);
                                                ?>
                                                <div class="card card-utilities">
                                                    <?php
                                                    $post_categories = get_post_primary_category($post->ID, 'category');
                                                    $primary_category = $post_categories['primary_category'];
                                                    ?>
                                                    <div class="card-img-wrapper">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                            <?php if (get_field('sponsor_name')): ?>
                                                                <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                            <?php endif; ?>
                                                        </a>
                                                    </div>

                                                    <div class="card-body">
                                                        <p><?php echo $primary_category->name; ?></p>

                                                        <h4 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                                        <?php if (has_excerpt()): ?>
                                                            <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 20, $more = '...'); ?>
                                                            <p><?php echo $excerpt_text; ?></p>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="card-footer">
                                                        <div class="article-stats nav-list">
                                                            <ul>
                                                                <li class="article-author"><?php the_author(); ?>
                                                                </li>
                                                                <li class="article-date"><?php the_date(); ?></li>
                                                                <li class="article-comments">
                                                                    <span class="icon-inline icon-comments-green">
                                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                                    </span>
                                                                    <span><?php echo $comments_count; ?></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php
                                            endforeach;
                                            wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="side-bot-ad">
                                <aside class="sidebar ad">
                                    <div class="row no-gutters row-deep">
                                        <div class="col-12 align-self-start">
                                            <!-- Advertisement: Desktop > 1024 -->
                                            <div class="hide-tablet">
                                                <div class="advert-content content-block double-margin">
                                                    <!-- Async AdSlot 3 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[300,600],[300,250]] -->
                                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[2]]) -->
                                                    <div id='div-gpt-ad-1420540-3' style="background-color: black;display: block;height: 300px;">
                                                      <script>
                                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-3'); });
                                                      </script>
                                                    </div>
                                                    <!-- End AdSlot 3 -->
                                                </div>
                                            </div>
                                            <!-- /Advertisement: Desktop > 1024 -->
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="utilities" class="visible-tablet content-block">
                <div class="article-utilities full-width">
                    <div class="row no-gutters">
                        <div class="col">
                            <a href="" title="Save article" class="bttn bttn-icon-save">
                                <span><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save article icon" /></span>
                                Save article
                            </a>
                        </div>

                        <div class="col-auto">
                            <div class="sharing-links">
                                <?php echo do_shortcode('[ssba-buttons]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="utilities" class="hide-tablet content-block">
                <div class="article-utilities">
                    <div class="row no-gutters">
                        <div class="col">
                            <a href="#" title="Save article" class="bttn bttn-icon-save">
                                <span><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save article icon" /></span>
                                Save article
                            </a>
                            <a href="" title="Print article" class="bttn bttn-icon-print">
                                <span><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/print-white.svg" alt="Print icon" /></span>
                                Print
                            </a>
                        </div>

                        <div class="col-auto">
                            <div class="social-sharing-links">
                                <div class="row no-gutters">
                                    <div class="col-auto">
                                        <div class="sharing-title text-standard">
                                            <h4 class="heading-std-text">Share this recipe</h4>
                                        </div>
                                    </div>

                                    <div class="col-auto">
                                        <div class="sharing-links">
                                            <?php echo do_shortcode('[ssba-buttons]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="article-main">
                <div class="row no-gutters">
                    <div class="visible-tablet">
                        <!-- Advertisement -->
                        <div class="col-12">
                            <div class="advert-content content-block double-margin">
                                <div class="row justify-content-center no-gutters">
                                    <div class="col">
                                        <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                        <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                        <div id='div-gpt-ad-1420540-2'>
                                          <script>
                                            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                          </script>
                                        </div>
                                        <!-- End AdSlot 2 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8 col-lg-9">
                        <div class="rate-review text-standard">
                            <h2>Comments</h2>

                            <div class="reviews">
                                <p>Reviews</p>

                                <?php
                                /*$comment_form_args = array(
                                    'title_reply'       => __( '' ),
                                    'title_reply_to'    => __( 'Leave a review to %s' ),
                                    'cancel_reply_link' => __( 'Cancel Review' ),
                                    'label_submit'      => __( 'Submit' ),
                                    'format'            => 'xhtml',

                                    'comment_field' =>  '<p class="comment-form-comment"><label for="comment">'._x('', 'noun').'</label><textarea id="comment" name="comment" placeholder="Something to say? Tell us what you loved about this. Inspire others, get typing" cols="45" rows="8" aria-required="true">'.'</textarea></p>',

                                    'must_log_in' => '<p class="must-log-in">' .
                                        sprintf(
                                            __('You must be <a href="%s">logged in</a> to post a comment.'),
                                            wp_login_url(apply_filters('the_permalink', get_permalink()))
                                        ) . '</p>',

                                    'logged_in_as' => '<p class="logged-in-as">' .
                                        sprintf(
                                            __('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>'),
                                            admin_url('profile.php'),
                                            $user_identity,
                                            wp_logout_url(apply_filters('the_permalink', get_permalink()))
                                        ) . '</p>',
                                );

                                comment_form($comment_form_args);*/
                                ?>
                            </div>

                            <?php
                            // If comments are open or we have at least one comment, load up the comment template.
                            if (comments_open() || get_comments_number()):
                                ?>
                                <div class="comment-wrapper">
                                    <?php comments_template('/comments-custom.php'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <!-- Advertisement -->
                    <div class="col-12">
                        <div class="advert-content content-block double-margin">
                            <div class="row justify-content-center no-gutters">
                                <div class="col">
                                    <!-- Async AdSlot 4 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[3]]) -->
                                    <div id='div-gpt-ad-1420540-4'>
                                      <script>
                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-4'); });
                                      </script>
                                    </div>
                                    <!-- End AdSlot 4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <div class="col-12">
                        <div class="content-block double-margin">
                            <div class="OUTBRAIN" data-src="<?php echo get_permalink(); ?>" data-widget-id="AR_1" data-ob-template="RadioTimes"></div>
                            <script type="text/javascript" async="async" src="https://widgets.outbrain.com/outbrain.js"></script>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

<?php get_footer();
