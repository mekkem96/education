<?php
/**
 * Template Name: Preferences
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="t_header_placeholder"></div>
<div class="content-wrapper">
	<div class="t_preference_choose">
		<div class="row">
			<div class="col-12 t_preference_choose_txt">
				<h1>Great! Any preference?</h1>
				<p>Tell us what food you love and we’ll send you fresh <br>ideas into your inbox once a week!</p>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox1" /> 
					<label for="checkbox1">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Baking recipes and tips
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>			
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox2" /> 
					<label for="checkbox2">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Healthy recipes
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox3" /> 
					<label for="checkbox3">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Dinner party menus
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox4" /> 
					<label for="checkbox4">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Cheap eats
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox5" /> 
					<label for="checkbox5">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Quick midweek meals
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox6" /> 
					<label for="checkbox6">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Seasonal recipes
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox7" /> 
					<label for="checkbox7">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Occasion meals
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox8" /> 
					<label for="checkbox8">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Vegetarian recipes
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox9" /> 
					<label for="checkbox9">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Exclusive promotions  and competitions
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="t_prefcheck">
					<input type="checkbox" name="radio" id="checkbox10" /> 
					<label for="checkbox10">
						<div class="t_image" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/t_choose.png) no-repeat; background-size: cover;">
							
						</div>
						<div class="t_content">
							<h3>
								Food news
							</h3>
							<small>421 Recipes</small>
						</div>
					</label>
				</div>
			</div>
			<div class="col-12 text-center t_preference_button">
				<div>
					<a href="#" class="greenbutton">Save my preferences</a>
				</div>
				<div>
					<a href="#" class="greenborder">No, thanks. I'll do it later</a>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer();
