<?php
/**
 * Template Name: Account Edit
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
$user_id=get_current_user_id();

//print_r($_SERVER);
if ( is_user_logged_in()  ) {
    if($_POST['ID'] !== $user_id)
        $_POST['ID'] = $user_id;
    //print_r($_POST);
    if(!empty($_POST['phone'] )&& !empty($_POST['postcode']) && !empty($_POST['user_email']) && !empty($_POST['last_name']) && !empty($_POST['first_name']) ){
        wp_update_user( $_POST );
        update_user_meta( $user_id, 'phone', $_POST['phone'] );
        update_user_meta( $user_id, 'postcode', $_POST['postcode'] );
        wp_update_user( array( 'ID' => $user_id, 'user_email' => esc_attr( $_POST['user_email'] ) ) );
        update_user_meta($user_id, 'user_email', $_POST['user_email']  );
        $_POST = array();
    }
    $user_post = $post->ID;
    $profile = get_userdata( $user_id );

    //print_r($profile);

    if ( $profile )
        $profile->filter = 'edit';
    $result = wp_check_password($_POST['pass1'], $profile->user_pass, $profile->ID);

    if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'])  && !empty( $_POST['pass3']) ) {
        if ( $_POST['pass3'] == $_POST['pass2'] && $result == 1 )
            wp_update_user( array( 'ID' => $user_id, 'user_pass' => esc_attr( $_POST['pass2'] ) ) );
        else
            wp_redirect( get_permalink() . '?error' );
        exit;
    }
    ?>
    <?php
    /* If profile was saved, update profile. */
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['updateuser'] ) && $_POST['updateuser'] == 'Save updates' ) {
        // These files need to be included as dependencies when on the front end.
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        //echo "string";
        // Let WordPress handle the upload.
        $img_id = media_handle_upload('avatar_user', $user_id );
        //print_r( $img_id);

        if ( is_wp_error( $img_id ) ) {
            echo "Error";
        } else {
            update_user_meta( $user_id, 'image',$img_id);
        }
    }
    ?>


    <div class="t_header_placeholder"></div>
    <div class="content-wrapper t_account ">




        <form method="post"  enctype='multipart/form-data'><?php
            $id_image = get_the_author_meta('image',$user_id);
            $id_image2 =  wp_get_attachment_image($id_image);
            do_action( 'user_edit_form_tag' );
            ?>
            <div class="t_account_form_wrap">
                <div class="row">




                    <div class="col-12 t_backmargin">
                        <a href="/account" class="t_back">Back to your account</a>
                    </div>
                    <div class="col-12 col-lg-8">
                        <h2 class="mb17">About you</h2>
                        <div class="t_profile_box">
                            <div>
                                <div class="t_profile_image">
                                    <?php
                                    if($id_image){
                                        echo $id_image2 ?>
                                    <?php }else{ ?>
                                        <img src="http://2.gravatar.com/avatar/86b15a9b43ee1bbcd743d4e25dec1c1f?s=26&d=mm&r=g" alt="">
                                    <?php } ?>


                                </div>
                                <div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6">


                                            <div href="#" id="uploads" class="greenborder inputfile">
                                                <input class="text-input" name="avatar_user" type="file" id="avatar_user" value="<?php the_author_meta( 'image', $current_user->ID ); ?>" />
                                                <!--<input class="text-input" name="avatar_user" type="file" id="avatar_user" multiple="false"/>-->
                                                Update your photo
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <a href="#" class="greenborder">Remove your photo</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row t_form_styler">
                            <div class="col-6">
                                <div class="t_account_line">
                                    <label for="name">First name</label>
                                    <input type="text"  name="first_name" id="first_name" value="<?php echo esc_attr($profile->first_name) ?>" class="regular-text" >
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="t_account_line">
                                    <label for="lastname">Last name</label>
                                    <input type="text"   name="last_name" id="last_name" value="<?php echo esc_attr($profile->last_name) ?>" class="regular-text">
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="t_account_line">
                                    <label for="postcode">Postcode</label>
                                    <input type="text" id="postcode" name="postcode" value="<?php echo esc_attr( get_the_author_meta( 'postcode', $profile->ID ) ); ?>" >
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="t_account_line">
                                    <label for="tel">Telephone Number</label>
                                    <input type="tel" id="tel" name="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $profile->ID ) ); ?>" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 t_form_styler">
                        <h2>Update your email</h2>
                        <div class="t_account_line email">
                            <label for="email">Email</label>
                            <input type="email"  name="user_email" id="email" value="<?php echo esc_attr( $profile->user_email ) ?>" class="regular-text ltr" >
                        </div>

                        <h2>Reset your password</h2>
                        <div class="t_account_line">
                            <label for="password">Current password</label>
                            <input type="password" id="password" name="pass1" >
                        </div>
                        <div class="t_account_line">
                            <label for="password2">New password</label>
                            <input type="password" id="password2"  name="pass2" >
                        </div>

                        <div class="t_account_line">
                            <label for="password3">Confirm new password</label>
                            <input type="password" id="password3" name="pass3">
                        </div>
                    </div>


                </div>
                <div class="col-12 text-center t_preference_button">
                    <input name="updateuser" type="submit" id="updateuser" class="greenbutton" value="<?php _e('Save updates', 'profile'); ?>" />

                </div>

                <?php// wp_update_user( $profile );  ?>
            </div>

        </form>


        <?php //echo get_the_author_meta( 'image', $profile->ID );echo md5( 'pC@fQEck0J9yCRrx0rfnh&BT' );

        if ( !empty( $_POST['avatar_user'] ) )  update_user_meta( $user_id, 'image', esc_attr( $_POST['avatar_user'] ) );

        $terms = get_terms( array(
            'taxonomy' => 'cuisine',
            'hide_empty' => false,  ) );

        //$cuisines = get_field('cuisine', 'user_'.$user_id );
        //$user = wp_get_current_user();
        $aass = get_user_meta($user_id , 'cuisine', true);
        //print_r($aass);
        ?>

        <div class="t_preference_choose">
            <form action=""  method="POST" >
                <?php


                $term_update = $_POST;
                foreach ($term_update as $key => $value) {
                    if( $value == 'on' ){
                        $term_updated[] = $key;
                    }
                }


                if($term_updated){
                    //add_user_meta ($user_id, 'cuisine', $term_updated);
                    update_user_meta($user_id, 'cuisine',  $term_updated);
                }

                ?>
                <div class="row">
                    <div class="col-12 t_preference_choose_txt">
                        <h2>Your preferences</h2>
                        <p>Tell us what food you love and we’ll send you fresh <br>ideas into your inbox once a week!</p>
                    </div>

                    <?php
                    $cuisines = get_field('cuisine', 'user_'.$user_id );

                    foreach($terms as $term):
                    $check = '';
                    $images = get_field('image', $term);
                    if($cuisines)
                        foreach($cuisines as $cuisine){

                            if($term->term_id === $cuisine->term_id) $check = 'checked';
                        }

                    ?>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="t_prefcheck">
                            <input type="checkbox" name="<?php echo $term->term_id; ?>" id="<?php echo $term->term_id; ?>" <?php echo $check; ?> user_id = "<?php echo $user_id ; ?>"  term_id = "<?php echo $term->term_id; ?>"  />
                            <label for="<?php echo $term->term_id; ?>">
                                <div class="t_image" style="background: url("<?php echo $images;  ?>") no-repeat; background-size: cover;">

                        </div>
                        <div class="t_content">
                            <h3>
                                <?php echo $term->name;?>
                                <?php// echo print_r($term) ;?>
                            </h3>
                            <small><?php $term->count;?> Recipes</small>
                        </div>
                        </label>
                    </div>
                </div>
            <?php    // Show Posts ...
            endforeach;?>

                <div class="col-12 text-center t_preference_button">
                    <a href="#" class="greenbutton style-2">Cancel</a>
                    <input type="submit" class="greenbutton" value="Save updates" >
                </div>
        </div>
        </form>
    </div>
    </div>





    <?php
}
?>
<?php
/* Adding Image Upload Fields */
add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

?>
    <script type="text/javascript">

        $( "#uploads" ).click(function() {
            console.log("dssss");

            $( "#avatar_user" ).trigger( "click" );
            //update( $( "span:last" ) );
            myVar = setTimeout(alertFunc, 15000);

            function alertFunc() {
                $( "#updateuser" ).trigger( "click" );
            }
        });

    </script>
    <style>
        .inputfile {
            position: relative;
        }
        .inputfile:hover, .inputfile input:hover {
            cursor: pointer !important;
        }
        .inputfile input[type="file"] {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0;
        }
        #adsduser{
            display: none;
        }
    </style>
<?php get_footer();