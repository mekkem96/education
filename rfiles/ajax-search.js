jQuery(document).ready(function() {
    //console.log('search js added');
    var setDefault = function() {
        // console.log('default view');
        jQuery('.t_searchpopular').removeClass('t_searchpopular--hidden');
        jQuery('.t_searchresult').addClass('t_searchresult--hidden');
    };
    var showResult = function() {
        jQuery('.t_searchpopular').addClass('t_searchpopular--hidden');
        jQuery('.t_searchresult').removeClass('t_searchresult--hidden');
    };

    jQuery('#search_field').bind( 'keypress', function() {
        //console.log('ajax start to send');
        var input = $(this);
        if ( input.val().length > 2 ) {
            jQuery('.t_search_box').addClass('t_search_box--loading');

            var formObj = {
                action:         "ajax_search_callback",
                search_query:   input.val()
            };

            $.post( search_obj.ajax_url, formObj, function(data) {
                if ( data ) {
                    // console.log('AJAX success');

                    /*console.log(data);*/
                    jQuery('.t_searchresult').html(data);
                    jQuery('.t_search_box').removeClass('t_search_box--loading');
                    showResult();
                } else {
                    // console.log('AJAX error');
                    setDefault();
                    jQuery('.t_search_box').removeClass('t_search_box--loading');
                }
            });


        } else {
            setDefault();
        }
    }).blur(function() {
        var input = $(this);
        if ( input.val().length < 3 ) {
            setDefault();
        }
    });
});