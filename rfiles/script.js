(function($){
    $(document).ready(function() {
        /* removing a weird blink from the search btn */
        $('#desctopsearch a').click(function() {
            return false;
        });

        var $grid = $('.search_grid').isotope({
            itemSelector: '.search_item',
            layoutMode: 'masonry',
            getSortData: {
                name: '.name',
                symbol: '.symbol',
                number: '.number parseInt',
                category: '[data-category]',
                weight: function( itemElem ) {
                    var weight = $( itemElem ).find('.weight').text();
                    return parseFloat( weight.replace( /[\(\)]/g, '') );
                }
            }
        });

        // bind filter button click
        $('#filters').on( 'click', 'button', function() {
            var filterValue = $( this ).attr('data-filter');
            // use filterFn if matches value
            $grid.isotope({ filter: filterValue });
        });

        // bind sort button click
        $('#sorts').on( 'click', 'button', function() {
            var sortByValue = $(this).attr('data-sort-by');
            $grid.isotope({ sortBy: sortByValue });
        });

        // change is-checked class on buttons
        $('.button-group').each( function( i, buttonGroup ) {
            var $buttonGroup = $( buttonGroup );
            $buttonGroup.on( 'click', 'button', function() {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $( this ).addClass('is-checked');
            });
        });

        $('.filter_box p').click(function() {
            if ($(this).parent().hasClass('filter_box--active')) {
                $(this).parent().removeClass('filter_box--active');
            } else {
                $(this).parent().addClass('filter_box--active');
            }
        });
        // console.log('script added');
        /*$('header.hide-mobile.sticky-header').bind('change', function() {
         console.log('header resized');
         });*/

        $(window).bind('load', function() {
            // console.log('ok');
            var target = $('button.elm-button').offset().top;
            var winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            var doc = document.documentElement,
                topPoint = 0,
                callBackCount = 0;
            // console.log(target);

            $('button.elm-button').bind('click', function() {
                setTimeout(function() {
                    var interVal = setInterval(function() {
                        if ($('button.elm-button').is(':visible')) {
                            if ($('button.elm-button').hasClass('is-loading')) {
                                // console.log('loading');
                            } else {
                                setTimeout(function() {
                                    console.log('loaded, callback refreshed');
                                    target = $('button.elm-button').offset().top;
                                    callBackCount = 0;
                                    // console.log(target);
                                    clearInterval(interVal);
                                }, 600);
                            }
                        } else {
                            clearInterval(interVal);
                        }
                    }, 1000);
                }, 300);
            });

            window.addEventListener('scroll', function() {
                topPoint = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
                if (topPoint >= target - winHeight && callBackCount === 0) {
                    // console.log('ajax running');
                    callBackCount = 1;
                    $('button.elm-button').trigger('click');
                }
            });
        });

    });
})(jQuery);