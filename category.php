<?php
/**
 * The category archive template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section>
                <div class="row no-gutters">
                    <div class="col-sm-12 col-md-6">
                        <div class="recipe-heading text-standard">
                            <div class="recipe-title">
                                <h1><?php single_cat_title(); ?></h1>
                                <?php echo category_description(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col order-md-last">
                        <?php if( get_field('sponsor_name') ): ?>
                            <div class="sponsor">
                                <div class="sponsor-text">Sponsored by<br><?php the_field('sponsor_name'); ?></div>

                                <div class="sponsor-logo">
                                    <?php
                                    $sponsor_logo = get_field('sponsor_logo');

                                    if(!empty($sponsor_logo)): ?>
                                        <img src="<?php echo $sponsor_logo['url']; ?>" alt="<?php echo $sponsor_logo['alt']; ?>" class="img-responsive" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row no-gutters">
                    <div class="col content-block">
                        <div class="filters">
                            <?php $category = get_category(get_query_var('cat')); ?>
                            <p class="count"><?php echo $category->count; ?> articles</p>

                            <div class="layout-options">
                                <span class="double"></span>
                                <span class="single"></span>
                            </div>
                        </div>

                        <div class="article-list">
                            <?php
                            $cat_args = array(
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'nopaging' => true,
                                'cat' => $category->term_id
                            );
                            $the_query = new WP_Query($cat_args);

                            $args = array(
                                'post_id' => get_the_ID(), // use post_id, not post_ID
                                'count' => true //return only the count
                            );
                            $comments_count = get_comments($args);

                            if ($the_query->have_posts()):
                                $count = 0;

                                while (have_posts()) : the_post();
                                    $count++;

                                    if ($count === 6):
                                    ?>
                                        <section style="float: left; width: 100%;">
                                            <div class="row no-gutters">
                                                <!-- Advertisement -->
                                                <div class="col-12">
                                                    <div class="advert-content content-block double-margin">
                                                        <div class="row justify-content-center no-gutters">
                                                            <div class="col">
                                                                <!-- Async AdSlot 4 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                                                <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[3]]) -->
                                                                <div id='div-gpt-ad-1420540-4'>
                                                                    <script>
                                                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-4'); });
                                                                    </script>
                                                                </div>
                                                                <!-- End AdSlot 4 -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    <?php else : ?>
                                        <div <?php post_class('article-card card-utilities'); ?>>
                                            <?php
                                            $post_categories = get_post_primary_category($post->ID, 'category');
                                            $primary_category = $post_categories['primary_category'];
                                            ?>

                                            <div class="card-img-wrapper">
                                                <a href="<?php the_permalink(); ?>">
                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/podcast-icon.svg" alt="Podcast icon" class="podcast-icon" />

                                                    <?php if (get_field('sponsor_name')): ?>
                                                        <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                    <?php endif; ?>
                                                </a>
                                            </div>

                                            <div class="card-body">
                                                <?php
                                                // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                                $taxonomy = 'category';
                                                $category = get_the_terms(get_the_ID(), $taxonomy);

                                                // If post has a category assigned.
                                                if ($category):
                                                    $category_display = '';

                                                    if (class_exists('WPSEO_Primary_Term')):
                                                        // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                                        $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_ID());
                                                        $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                                        $term = get_term($wpseo_primary_term);

                                                        if (is_wp_error($term)):
                                                            // Default to first category (not Yoast) if an error is returned
                                                            $category_display = $category[0]->name;
                                                        else:
                                                            // Yoast Primary category
                                                            $category_display = $term->name;
                                                        endif;
                                                    else:
                                                        // Default, display the first category in WP's list of assigned categories
                                                        $category_display = $category[0]->name;
                                                    endif;

                                                    echo '<p>'.$category_display.'</p>';
                                                endif;
                                                ?>

                                                <p class="podcast-text">Delicious. Podcast</p>

                                                <h4 class="card-title heading-std-text"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

                                                <?php if (has_excerpt()): ?>
                                                    <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 20, $more = '...'); ?>
                                                    <p><?php echo $excerpt_text; ?></p>
                                                <?php endif; ?>
                                            </div>

                                            <div class="card-footer">
                                                <div class="article-stats nav-list">
                                                    <ul>
                                                        <li class="article-author"><?php the_author(); ?></li>
                                                        <li class="article-date"><?php the_date(); ?></li>
                                                        <li class="article-comments">
                                                            <span class="icon-inline icon-comments-green">
                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                            </span>
                                                            <span><?php echo $comments_count; ?></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    endif;
                                endwhile;
                            else:
                                echo '<p>There are currently no articles</p>';
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

<?php get_footer();
