<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
$authorid = get_current_user_id();
?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section>
                <div class="row no-gutters">
                    <div class="col-sm-12">
                        <?php // HERO SECTION ?>
                        <div class="hero">
                            <div class="row">
                                <div class="hero-recipe col-sm-12 col-md-6">
                                    <?php
                                    $posts = get_field('recipe');

                                    if($posts):
                                        foreach($posts as $post): // variable must be called $post (IMPORTANT)
                                            setup_postdata($post);
                                            ?>
                                                <a href="<?php the_permalink(); ?>" class="card card-bg card-utilities">
                                                    <div class="card-img-wrapper">
                                                        <div class="cta-icon icon-only cta-icon-not-saved-white cta-save-recipe" post_id="<?php echo $post->ID;?>" user_id="<?php echo $authorid;?>">
                                                            <span><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save recipe icon" /></span>
                                                        </div>

                                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                        <?php if (get_field('sponsor_name')): ?>
                                                            <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="card-body">
                                                        <div class="card-meta">
                                                            <?php
                                                            // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                                            $taxonomy = 'collections';
                                                            $category = get_the_terms(get_the_ID(), $taxonomy);

                                                            // If post has a category assigned.
                                                            if ($category):
                                                                $category_display = '';

                                                                if (class_exists('WPSEO_Primary_Term')):
                                                                    // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                                                    $wpseo_primary_term = new WPSEO_Primary_Term('collections', get_the_ID());
                                                                    $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                                                    $term = get_term($wpseo_primary_term);

                                                                    if (is_wp_error($term)):
                                                                        // Default to first category (not Yoast) if an error is returned
                                                                        $category_display = $category[0]->name;
                                                                    else:
                                                                        // Yoast Primary category
                                                                        $category_display = $term->name;
                                                                    endif;
                                                                else:
                                                                    // Default, display the first category in WP's list of assigned categories
                                                                    $category_display = $category[0]->name;
                                                                endif;

                                                                echo '<span class="related-collection">'.$category_display.'</span>';
                                                            endif;
                                                            ?>

                                                            <div class="recipe-stats nav-list">
                                                                <ul>
                                                                    <li class="recipe-rating"><?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?></li>
                                                                    <li class="recipe-skill">
                                                                        <span class="icon-inline icon-effort-main">
                                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                                                                        </span>

                                                                        <?php if(get_field('difficulty') == 'Easy'): ?>
                                                                            <span>Easy</span>
                                                                        <?php elseif (get_field('difficulty') == 'Medium'): ?>
                                                                            <span>Medium</span>
                                                                        <?php elseif (get_field('difficulty') == 'Challenge'): ?>
                                                                            <span>A challenge</span>
                                                                        <?php endif; ?>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                        <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                        <?php $teaser_text = wp_trim_words(get_field('teaser_large'), $num_words = 25, $more = '...'); ?>
                                                        <p><?php echo $teaser_text; ?></p>
                                                    </div>
                                                </a>
                                            <?php
                                        endforeach;

                                        wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                    endif;
                                    ?>
                                </div>

                                <div class="hero-articles col-sm-12 col-md-6">
                                    <?php
                                    $posts = get_field('articles');

                                    $args = array(
                                        'post_id' => get_the_ID(), // use post_id, not post_ID
                                        'count' => true //return only the count
                                    );
                                    $comments_count = get_comments($args);

                                    if (count($posts) == '2'){
                                        $count_class = 'double';
                                    } else {
                                        $count_class = '';
                                    }

                                    if($posts):
                                        foreach($posts as $post): // variable must be called $post (IMPORTANT)
                                            setup_postdata($post);
                                            //$post = get_post();
                                            ?>
                                                <a href="<?php the_permalink(); ?>" <?php post_class(array('article card-bg card-utilities', $count_class)); ?>>
                                                    <div class="card-img-wrapper">
                                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/podcast-icon.svg" alt="Podcast icon" class="podcast-icon" />

                                                        <?php if (get_field('sponsor_name')): ?>
                                                            <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="card-body">
                                                        <?php
                                                        // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                                        $taxonomy = 'category';
                                                        $category = get_the_terms(get_the_ID(), $taxonomy);

                                                        // If post has a category assigned.
                                                        if ($category):
                                                            $category_display = '';

                                                            if (class_exists('WPSEO_Primary_Term')):
                                                                // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                                                $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_ID());
                                                                $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                                                $term = get_term($wpseo_primary_term);

                                                                if (is_wp_error($term)):
                                                                    // Default to first category (not Yoast) if an error is returned
                                                                    $category_display = $category[0]->name;
                                                                else:
                                                                    // Yoast Primary category
                                                                    $category_display = $term->name;
                                                                endif;
                                                            else:
                                                                // Default, display the first category in WP's list of assigned categories
                                                                $category_display = $category[0]->name;
                                                            endif;

                                                            echo '<p class="article-cat">'.$category_display.'</p>';
                                                        endif;
                                                        ?>

                                                        <p class="podcast-text">Delicious. Podcast</p>

                                                        <div class="card-meta">
                                                            <div class="article-stats nav-list">
                                                                <ul>
                                                                    <li class="article-author"><?php the_author(); ?></li>
                                                                    <li class="article-date"><?php the_date(); ?></li>
                                                                    <li class="article-comments">
                                                                        <span class="icon-inline icon-comments-green">
                                                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                                        </span>
                                                                        <span><?php echo $comments_count; ?></span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                        <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                        <?php if (has_excerpt()): ?>
                                                            <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 15, $more = '...'); ?>
                                                            <p><?php echo $excerpt_text; ?></p>
                                                        <?php endif; ?>
                                                    </div>
                                                </a>
                                        <?php
                                        endforeach;

                                        wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php if(get_field('show_featured_collection')): ?>
                <section>
                    <div class="row no-gutters">
                        <div class="col-sm-12">
                            <?php
                            // FEATURED COLLECTION
                            $collection_term = get_field('select_recipe_collection');
                            ?>

                            <div class="featured-collection content-block double-margin">
                                <div class="row no-gutters">
                                    <div class="col-md-12">
                                        <div class="intro-text text-standard">
                                            <?php if($collection_term): ?>
                                                <h2><?php echo $collection_term->name; ?></h2>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <?php
                                        $promoted_collection_args = array(
                                            'post_type' => 'recipe',
                                            'post_status' => 'publish',
                                            'posts_per_page' => -1,
                                            'tax_query' => array(
                                                array(
                                                    'taxonomy' => $collection_term->taxonomy,
                                                    'field'    => 'slug',
                                                    'terms'    => $collection_term->slug,
                                                ),
                                            ),
                                        );
                                        $the_query = new WP_Query($promoted_collection_args);
                                        ?>

                                        <div class="recipe-slider">
                                            <?php
                                            if ($the_query->have_posts()) :
                                                while ($the_query->have_posts() ) : $the_query->the_post();
                                                    ?>
                                                    <div <?php post_class('latest-recipe-card'); ?>>
                                                        <div class="card-bg card-utilities">
                                                            <div class="card-img-wrapper">
                                                                <a href="#" class="cta-icon icon-only cta-icon-not-saved-white">
                                                                    <span>
                                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save recipe icon" />
                                                                    </span>
                                                                </a>

                                                                <a href="<?php the_permalink(); ?>">
                                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top">
                                                                </a>
                                                            </div>

                                                            <div class="card-body">
                                                                <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                                <?php $teaser_text = wp_trim_words(get_field('teaser_large'), $num_words = 15, $more = '...'); ?>
                                                                <p><?php echo $teaser_text; ?></p>
                                                            </div>

                                                            <div class="card-footer">
                                                                <div class="recipe-stats nav-list">
                                                                    <ul>
                                                                        <li class="recipe-rating">
                                                                            <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                                                                        </li>
                                                                        <li class="recipe-skill">
                                                                            <span class="icon-inline icon-effort-main">
                                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                                                                            </span>
                                                                            <?php if(get_field('difficulty') == 'Easy'): ?>
                                                                                <span>Easy</span>
                                                                            <?php elseif (get_field('difficulty') == 'Medium'): ?>
                                                                                <span>Medium</span>
                                                                            <?php elseif (get_field('difficulty') == 'Challenge'): ?>
                                                                                <span>A challenge</span>
                                                                            <?php endif; ?>
                                                                        </li>
                                                                        <li class="recipe-comments">
                                                                            <span class="icon-inline icon-comments-green">
                                                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                                            </span>

                                                                            <?php
                                                                            $args = array(
                                                                                'post_id' => get_the_ID(), // use post_id, not post_ID
                                                                                'count' => true //return only the count
                                                                            );
                                                                            $comments_count = get_comments($args);
                                                                            ?>
                                                                            <span><?php echo $comments_count; ?></span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                endwhile;

                                                wp_reset_postdata();
                                            endif;
                                            ?>
                                        </div>

                                        <a href="#" title="Find more seasonal recipes" class="bttn">Find more seasonal recipes</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

            <section>
                <div class="row no-gutters">
                    <!-- Advertisement -->
                    <div class="col-12">
                        <div class="advert-content content-block double-margin">
                            <div class="row justify-content-center no-gutters">
                                <div class="col">
                                    <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                    <div id='div-gpt-ad-1420540-2'>
                                      <script>
                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                      </script>
                                    </div>
                                    <!-- End AdSlot 2 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php
            $terms = get_field('collections');

            if($terms):
            ?>
            <section>
                <div class="row no-gutters">
                    <div class="col-sm-12">
                        <?php // COLLECTIONS SLIDER ?>
                        <div class="collections content-block double-margin">
                            <div class="row no-gutters">
                                <div class="col-md-12">
                                    <div class="intro-text text-standard">
                                        <h2>Recent collections</h2>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="collection-slider">
                                        <?php
                                        foreach($terms as $term):
                                            $term_description = $term->description;
                                        ?>
                                            <a href="<?php echo get_term_link($term); ?>" class="collection-card">
                                                <div class="card-bg card-utilities">
                                                    <div class="card-img-wrapper">
                                                        <?php
                                                        $image = get_field('image', $term);

                                                        if(!empty($image)): ?>
                                                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                        <?php endif; ?>

                                                        <?php if (get_field('sponsor_name', $term)): ?>
                                                            <p>Sponsored by <?php the_field('sponsor_name', $term); ?></p>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="card-body">
                                                        <h4 class="card-title heading-std-text"><?php echo $term->name; ?></h4>

                                                        <?php $description_text = wp_trim_words($term_description, $num_words = 8, $more = '...'); ?>
                                                        <p><?php echo $description_text; ?></p>
                                                    </div>

                                                    <div class="card-footer">
                                                        <p><?php echo $term->count; ?> Recipes</p>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php endif; ?>

            <section>
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-lg-9">
                        <?php // MAIN INGREDIENTS LIST ?>
                        <div class="main-ingredients content-block double-margin">
                            <div class="row no-gutters">
                                <div class="col-md-12">
                                    <div class="intro-text text-standard">
                                        <h2>Find recipes by ingredient</h2>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <?php
                                    // Get main ingredients terms
                                    $ingredients = get_terms('ingredients', array(
                                            'hide_empty' => false,
                                        )
                                    );

                                    foreach($ingredients as $ingredient):
                                        $term_link = get_term_link($ingredient);

                                        if (is_wp_error($term_link)) {
                                            continue;
                                        }
                                        ?>
                                        <a href="<?php echo $term_link; ?>" class="ingredient">
                                            <?php
                                            $image = get_field('image', $ingredient);

                                            if(!empty($image)): ?>
                                                <img class="svg" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                            <?php endif; ?>

                                            <h4 class="card-title heading-std-text"><?php echo $ingredient->name; ?></h4>
                                            <p><?php echo $ingredient->count; ?> Recipes</p>
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>

                        <?php // READ MORE ?>
                        <div class="read-more content-block double-margin">
                            <div class="row no-gutters">
                                <div class="col-md-12">
                                    <div class="intro-text text-standard">
                                        <h2>Read more</h2>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="promoted-articles">
                                        <?php
                                        $posts = get_field('promoted_articles');
                                        

                                        $args = array(
                                            'post_id' => get_the_ID(), // use post_id, not post_ID
                                            'count' => true //return only the count
                                        );
                                        $comments_count = get_comments($args);

                                        if($posts):
                                            foreach( $posts as $post): // variable must be called $post (IMPORTANT)
                                                setup_postdata($post);

                                                if ($post) {
                                                    $categories = get_the_category($post->ID);
                                                }
                                                ?>
                                                <a href="<?php the_permalink(); ?>" <?php post_class('card card-utilities'); ?>>
                                                    <div class="card-img-wrapper">
                                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/podcast-icon.svg" alt="Podcast icon" class="podcast-icon" />

                                                        <?php if (get_field('sponsor_name')): ?>
                                                            <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="card-body">
                                                        <?php
                                                        // SHOW PRIMARY CATEGORY OR TAXONOMY, OR FIRST CATEGORY
                                                        $taxonomy = 'category';
                                                        $category = get_the_terms(get_the_ID(), $taxonomy);

                                                        // If post has a category assigned.
                                                        if ($category):
                                                            $category_display = '';

                                                            if (class_exists('WPSEO_Primary_Term')):
                                                                // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
                                                                $wpseo_primary_term = new WPSEO_Primary_Term('category', get_the_ID());
                                                                $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
                                                                $term = get_term($wpseo_primary_term);

                                                                if (is_wp_error($term)):
                                                                    // Default to first category (not Yoast) if an error is returned
                                                                    $category_display = $category[0]->name;
                                                                else:
                                                                    // Yoast Primary category
                                                                    $category_display = $term->name;
                                                                endif;
                                                            else:
                                                                // Default, display the first category in WP's list of assigned categories
                                                                $category_display = $category[0]->name;
                                                            endif;

                                                            echo '<p class="article-cat">'.$category_display.'</p>';
                                                        endif;
                                                        ?>

                                                        <p class="podcast-text">Delicious. Podcast</p>

                                                        <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                        <?php if (has_excerpt()): ?>
                                                            <?php $excerpt_text = wp_trim_words(get_the_excerpt(), $num_words = 20, $more = '...'); ?>
                                                            <p><?php echo $excerpt_text; ?></p>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div class="card-footer">
                                                        <div class="article-stats nav-list">
                                                            <ul>
                                                                <?php $author = get_the_author(); ?>
                                                                <li class="article-author"><?php the_author(); ?></li>
                                                                <li class="article-date"><?php the_date(); ?></li>
                                                                <li class="article-comments">
                                                                    <span class="icon-inline icon-comments-green">
                                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/comments-green.svg" alt="Comment count icon" />
                                                                    </span>
                                                                    <span><?php echo $comments_count; ?></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </a>
                                                <?php
                                            endforeach;

                                            wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php // SIDEBAR ?>
                    <div class="col-sm-12 col-md-4 col-lg-3 hide-tablet">
                        <aside class="sidebar">
                            <div class="row no-gutters row-deep">
                                <div class="col-12 align-self-start">
                                    <!-- Advertisement: Desktop > 1024 -->
                                    <div class="hide-tablet">
                                        <div class="advert-content content-block double-margin">
                                            <!-- Async AdSlot 3 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[300,600],[300,250]] -->
                                            <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[2]]) -->
                                            <div id='div-gpt-ad-1420540-3'>
                                              <script>
                                                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-3'); });
                                              </script>
                                            </div>
                                            <!-- End AdSlot 3 -->
                                        </div>
                                    </div>
                                    <!-- /Advertisement: Desktop > 1024 -->
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <!-- Advertisement -->
                    <div class="col-12">
                        <div class="advert-content content-block double-margin">
                            <div class="row justify-content-center no-gutters">
                                <div class="col">
                                    <!-- Async AdSlot 4 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[3]]) -->
                                    <div id='div-gpt-ad-1420540-4'>
                                      <script>
                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-4'); });
                                      </script>
                                    </div>
                                    <!-- End AdSlot 4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                //    event.preventDefault();
                var page = 2;

                $('.cta-save-recipe').click(function(e) {
                    e.preventDefault(false);
                    return false;
                });

                $('.cta-save-recipe').click(function(){
                    var post_id = $(this).attr('post_id');
                    var user_id = $(this).attr('user_id');
                    var field = 'recipes';
                    $(this).parent().parent().addClass('recipe-card--loading');
                    /*console.log(post_id);
                     console.log(user_id);*/
                    $.ajax({
                        url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                        data: {
                            'action': 'get_recipes_save',
                            'post_id' : post_id,
                            'user_id' : user_id,
                            'field' : field
                        },
                        success:function(data) {
                            /*console.log(data);
                             console.log(post_id);*/
                            /*$('[id=' + post_id + ']').css('display','none');*/
                            $('div[post_id="' + post_id+'"]')
                                .parent().parent().removeClass('recipe-card--loading')
                                .find('span img').attr('src', '<?= esc_url(home_url('/wp-content/themes/delicious/assets/icons/saved-white.svg')) ?>');
                        }
                    });
                });
            });
        })(jQuery);
    </script>

<?php get_footer();