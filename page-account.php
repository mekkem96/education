<?php
/**
 * Template Name: Account
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();

if ( is_user_logged_in() ) {
    $author_id=get_current_user_id( );
    $first= get_the_author_meta( 'first_name', $author_id);
    $last= get_the_author_meta( 'last_name', $author_id);
    $display_name= get_the_author_meta( 'display_name', $author_id);
    $image = get_field('image', 'user_'.$author_id.'');
    ?>
    <div class="t_header_placeholder"></div>
    <div class="content-wrapper t_account ">

        <div class="t_account_profile_title">
            <div>
                <div>
                    <h1>
                        <?php
                        if($image){?>
                            <img src="<?php echo $image;?>" alt="">

                        <?php }else{ ?>
                            <img src="http://2.gravatar.com/avatar/86b15a9b43ee1bbcd743d4e25dec1c1f?s=26&d=mm&r=g" alt="">
                        <?php } ?>

                        <?php
                        echo "Hi, $display_name "; ?>
                    </h1>
                </div>
                <div class="hidden-md">
                    <a href="/account-edit" class="greenbutton style-2 edit">Edit your profile</a>
                    <a href="<?php echo wp_logout_url('/login'); ?>" class="greenbutton style-2">Log out</a>
                </div>

            </div>
        </div>
        <?php $posts = get_field('recipes', 'user_'.$author_id );
        $count = count($posts);
        if(empty($posts)) $count = 0;
        ?>
        <div class="t_accordion_head visible-md"><a href="#">Edit your profile</a> <span class="arrow"></span></div>
        <div class="t_accordion_head"><a href="#">My saved recipes</a> <span class="hidden-md">(<i class="recipes_count"><?php echo $count;?></i>)</span> <span class="arrow"></span></div>
        <div class="t_accordion_body">
            <div class="recipe-list">
                <?php
                if( $posts ): ?>
                    <?php foreach( $posts as $post):
                        $post = get_post($post); ?>
                        <div id ="<?php echo $post->ID; ?>" <?php post_class('recipe-card card-bg card-utilities'); ?>>
                            <div class="card-img-wrapper">
                                <div posts="<?php echo $post->ID; ?>" id="recipe" user_id="<?php echo $author_id;?>" class="cta-icon icon-only cta-icon-not-saved-white">
                                    <span>
                                       <img src="http://delicious.1devserver.co.uk/wp-content/themes/delicious/assets/images/t_close.png" alt="Save recipe icon">
                                    </span>
                                </div>
                                <a href="<?php echo get_permalink($post->ID); ?>">
                                    <img src="<?php echo get_the_post_thumbnail_url($post->ID);?>" alt="" class="img-responsive card-img-top">
                                </a>
                            </div>

                            <div class="card-body">
                                <h4 class="card-title heading-std-text"><a href="<?php echo get_permalink($post->ID); ?>"><?php echo get_the_title($post->ID); ?></a></h4>
                            </div>

                            <div class="card-footer">
                                <div class="recipe-stats nav-list">
                                    <ul>
                                        <li class="recipe-rating">
                                            <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                                        </li>
                                        <li class="recipe-skill">
                                            <span class="icon-inline icon-effort-main">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                                            </span>
                                            <?php if(get_field('difficulty') == 'Easy'): ?>
                                                <span>Easy</span>
                                            <?php elseif (get_field('difficulty') == 'Medium'): ?>
                                                <span>Medium</span>
                                            <?php elseif (get_field('difficulty') == 'Challenge'): ?>
                                                <span>A challenge</span>
                                            <?php endif; ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                <?php endif;?>
            </div>
        </div>
        <?php
        $collections = get_field('collection', 'user_'.$author_id );
        $count = count($collections);
        if(empty($collections)) $count = 0;
        ?>
        <div class="t_accordion_head"><a href="#">My saved collections</a> <span class="hidden-md">(<i class="collections_count"><?php echo $count; ?></i>)</span> <span class="arrow"></span></div>
        <div class="t_accordion_body">
            <div class="recipe-list">
                <?php
                if ( $collections ) :
                    foreach ( $collections as $collection) :
                        $acfID = $collection->taxonomy.'_'.$collection->term_id;?>

                        <div id ="<?php echo $collection->term_id;  ?>" <?php post_class('recipe-card card-bg card-utilities'); ?>>
                            <div class="card-img-wrapper">
                                <div posts="<?php echo $collection->term_id; ?>" id="collection" user_id="<?php echo $author_id;?>" class="cta-icon icon-only cta-icon-not-saved-white">
                                    <span>
                                       <img src="http://delicious.1devserver.co.uk/wp-content/themes/delicious/assets/images/t_close.png" alt="Save recipe icon">
                                    </span>
                                </div>
                                <?php if ((bool) $colImage = get_field( 'image', $acfID)) : ?>
                                    <a href="<?php echo get_category_link($collection->term_id); ?>">
                                        <img src="<?php echo $colImage['url'];?>" alt="<?php echo $colImage['alt'];?>" class="img-responsive card-img-top">
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title heading-std-text"><a href="<?php echo get_term_link($collection->term_id); ?>"><?php echo $collection->name; ?></a></h4>
                            </div>

                            <div class="card-footer">
                                <div class="recipe-stats nav-list">
                                    <ul>
                                        <li class="recipe-rating">
                                            <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                                        </li>
                                        <li class="recipe-skill">
                                            <span class="icon-inline icon-effort-main">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                                            </span>
                                            <?php
                                            if(get_field('difficulty', $acfID) == 'Easy'): ?>
                                                <span>Easy</span>
                                            <?php elseif (get_field('difficulty', $acfID) == 'Medium'): ?>
                                                <span>Medium</span>
                                            <?php elseif (get_field('difficulty', $acfID) == 'Challenge'): ?>
                                                <span>A challenge</span>
                                            <?php endif; ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                <?php endif;?>
            </div>
        </div>
        <div class="t_accordion_head visible-md"><a href="#">Log Out</a> <span class="arrow"></span></div>
    </div>
    <div class="t_clear"></div>

    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                //	event.preventDefault();
                var page = 2;

                $(document).on('click', '#recipe', function(e){
                    var post_id = $(this).attr('posts');
                    var user_id = $(this).attr('user_id');
                    var field = 'recipes';
                    $(this).parent().parent().addClass('recipe-card--loading');
                    console.log(post_id);
                    console.log(user_id);
                    console.log(field);
                    $.ajax({
                        url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                        data: {
                            'action': 'get_recipes_delete',
                            'post_id' : post_id,
                            'user_id' : user_id,
                            'field' : field,
                        },
                        success:function(data) {
                            console.log(data);
                            console.log(post_id);
                            $('[id=' + post_id + ']').removeClass('recipe-card--loading');
                            setTimeout(function(){
                                $('[id=' + post_id + ']').addClass('recipe-card--removing');

                                setTimeout(function(){
                                    $('[id=' + post_id + ']').remove();
                                }, 600);
                            }, 300);

                            /*$('[id=' + post_id + ']').css('display','none');*/
                            var curNum = parseInt($('.recipes_count').html()) - 1;
                            if (curNum >= 0)
                                $('.recipes_count').html(curNum);
                            /*console.log(curNum);*/
                        }
                    });
                });

                $(document).on('click', '#collection', function(e){
                    var post_id = $(this).attr('posts');
                    var user_id = $(this).attr('user_id');
                    var field = 'collection';
                    $(this).parent().parent().addClass('recipe-card--loading');
                    console.log(post_id);
                    console.log(user_id);
                    console.log(field);
                    $.ajax({
                        url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
                        data: {
                            'action': 'get_recipes_delete',
                            'post_id' : post_id,
                            'user_id' : user_id,
                            'field'   : field,
                        },
                        success:function(data) {
                            console.log(data);
                            console.log(post_id);

                            $('[id=' + post_id + ']').removeClass('recipe-card--loading');
                            setTimeout(function(){
                                $('[id=' + post_id + ']').addClass('recipe-card--removing');

                                setTimeout(function(){
                                    $('[id=' + post_id + ']').remove();
                                }, 600);
                            }, 300);

                            var curNum = parseInt($('.collections_count').html()) - 1;
                            if (curNum >= 0)
                                $('.collections_count').html(curNum);
                            console.log(curNum);
                        }
                    });
                });


            })
        })(jQuery);
    </script>

<?php }else{
    wp_redirect('/login');
    $post = array();
    //exit("sddddd");
}
?>
<?php get_footer();