<?php
/**
 * Template Name: Recipes
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
$authorid=get_current_user_id( );
?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section>
                <div class="row no-gutters">
                    <div class="col-sm-12 col-md-4">
                        <div class="recipe-heading text-standard">
                            <div class="recipe-title">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col order-md-last">
                        <?php if( get_field('sponsor_name') ): ?>
                            <div class="sponsor">
                                <div class="sponsor-text">Sponsored by<br><?php the_field('sponsor_name'); ?></div>

                                <div class="sponsor-logo">
                                    <?php
                                    $sponsor_logo = get_field('sponsor_logo');

                                    if(!empty($sponsor_logo)): ?>
                                        <img src="<?php echo $sponsor_logo['url']; ?>" alt="<?php echo $sponsor_logo['alt']; ?>" class="img-responsive" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <?php // LATEST RECIPES SLIDER ?>
                <div class="latest-recipes content-block">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="intro-text text-standard">
                                <h2>Latest recipes</h2>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="recipe-slider">
                                <?php
                                $args = array(
                                    'post_type' => 'recipe',
                                    'post_status' => 'publish',
                                    'posts_per_page' => 8,
                                );
                                $the_query = new WP_Query($args);

                                if ($the_query->have_posts()):
                                    while ($the_query->have_posts()) : $the_query->the_post();
                                        ?>
                                        <a href="<?php the_permalink(); ?>" class="latest-recipe-card">
                                            <div class="card-bg card-utilities rec-tab">
                                                <div class="card-img-wrapper">
                                                    <div class="cta-icon icon-only cta-icon-not-saved-white">
                                                        <span><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save recipe icon" /></span>
                                                    </div>

                                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="img-responsive card-img-top" />

                                                    <?php if (get_field('sponsor_name')): ?>
                                                        <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name'); ?></p>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="card-body">
                                                    <h4 class="card-title heading-std-text"><?php the_title(); ?></h4>

                                                    <?php $teaser_text = wp_trim_words(get_field('teaser_large'), $num_words = 15, $more = '...'); ?>
                                                    <p><?php echo $teaser_text; ?></p>
                                                </div>

                                                <div class="card-footer">
                                                    <div class="recipe-stats nav-list">
                                                        <ul>
                                                            <li class="recipe-rating">
                                                                <?php echo do_shortcode('[yasr_overall_rating size="small"]'); ?>
                                                            </li>
                                                            <li class="recipe-skill">
                                                                <span class="icon-inline icon-effort-main">
                                                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/effort-green.svg" alt="Effort icon" />
                                                                </span>
                                                                <?php if(get_field('difficulty') == 'Easy'): ?>
                                                                    <span>Easy</span>
                                                                <?php elseif (get_field('difficulty') == 'Medium'): ?>
                                                                    <span>Medium</span>
                                                                <?php elseif (get_field('difficulty') == 'Challenge'): ?>
                                                                    <span>A challenge</span>
                                                                <?php endif; ?>
                                                            </li>
                                                            <li class="recipe-comments">
                                                                <span class="icon-inline icon-comments-green"> <img src="http://deliciousnew.wpengine.com/wp-content/themes/delicious/assets/icons/comments-green.svg" alt="Comment count icon"> </span>
                                                                <?php
                                                                $args = array(
                                                                    'post_id' => get_the_ID(), // use post_id, not post_ID
                                                                    'count' => true //return only the count
                                                                );
                                                                $comments_count = get_comments($args);
                                                                ?>
                                                                <span><?php echo $comments_count; ?></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    <?php
                                    endwhile;

                                    wp_reset_postdata();
                                else:
                                    ?>
                                    <p><?php esc_html_e('Sorry, there are currently no related exhibitors.'); ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php // CUISINES SLIDER ?>
                <div class="cuisines content-block">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="intro-text text-standard">
                                <h2>Cuisines</h2>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <?php
                            // Get Cuisines terms
                            $cuisines = get_terms('cuisine', array(
                                    'hide_empty' => true,
                                )
                            );
                            ?>
                            <div class="cuisines-slider">
                                <?php
                                foreach($cuisines as $cuisine):
                                    $term_link = get_term_link($cuisine);
                                    $term_description = $cuisine->description;

                                    if (is_wp_error($term_link)) {
                                        continue;
                                    }
                                    ?>
                                    <a href="<?php echo $term_link; ?>" class="cuisine-card">
                                        <div class="card-bg card-utilities">
                                            <div class="card-img-wrapper">
                                                <div class="cta-icon icon-only cta-icon-not-saved-white"  post_id = "<?php echo $cuisine->term_id;?>" user_id  = "<?php echo $authorid;?>" >
                                                    <span>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save recipe icon" />
                                                    </span>
                                                </div>
                                                <?php
                                                $image = get_field('image', $cuisine);

                                                if(!empty($image)): ?>
                                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                                <?php endif; ?>
                                            </div>

                                            <div class="card-body">
                                                <h4 class="card-title heading-std-text"><?php echo $cuisine->name; ?></h4>

                                                <?php $description_text = wp_trim_words($term_description, $num_words = 10, $more = '...'); ?>
                                                <p><?php echo $description_text; ?></p>
                                            </div>

                                            <div class="card-footer">
                                                <p><?php echo $cuisine->count; ?> Recipes</p>
                                            </div>
                                        </div>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <section>
                    <div class="row no-gutters">
                        <!-- Advertisement -->
                        <div class="col-12">
                            <div class="advert-content content-block double-margin">
                                <div class="row justify-content-center no-gutters">
                                    <div class="col">
                                        <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                        <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                        <div id='div-gpt-ad-1420540-2'>
                                          <script>
                                            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                          </script>
                                        </div>
                                        <!-- End AdSlot 2 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <?php // MAIN INGREDIENTS LIST ?>
                <div class="main-ingredients content-block">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="intro-text text-standard">
                                <h2>Main ingredients</h2>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <?php
                            // Get main ingredients terms
                            $ingredients = get_terms('ingredients', array(
                                    'hide_empty' => false,
                                )
                            );

                            foreach($ingredients as $ingredient):
                                $term_link = get_term_link($ingredient);

                                if (is_wp_error($term_link)) {
                                    continue;
                                }
                                ?>
                                <a href="<?php echo $term_link; ?>" class="ingredient">
                                    <?php
                                    $image = get_field('image', $ingredient);

                                    if(!empty($image)): ?>
                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                    <?php endif; ?>

                                    <h4 class="card-title heading-std-text"><?php echo $ingredient->name; ?></h4>
                                    <p><?php echo $ingredient->count; ?> Recipes</p>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                <section>
                    <div class="row no-gutters">
                        <!-- Advertisement -->
                        <div class="col-12">
                            <div class="advert-content content-block double-margin">
                                <div class="row justify-content-center no-gutters">
                                    <div class="col">
                                        <!-- Async AdSlot 2 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                        <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[1]]) -->
                                        <div id='div-gpt-ad-1420540-2'>
                                          <script>
                                            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-2'); });
                                          </script>
                                        </div>
                                        <!-- End AdSlot 2 -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <?php // DIETARY REQUIREMENTS SLIDER ?>
                <div class="dietary-requirements content-block">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="intro-text text-standard">
                                <h2>Dietary requirements</h2>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <?php
                            // Get dietary requirements terms
                            $dietary_requirements = get_terms('dietary_requirements', array(
                                    'hide_empty' => false,
                                )
                            );
                            ?>
                            <div class="dietary-slider">
                                <?php
                                foreach($dietary_requirements as $dietary_requirement):
                                    $term_link = get_term_link($dietary_requirement);
                                    $term_description = $dietary_requirement->description;

                                    if (is_wp_error($term_link)) {
                                        continue;
                                    }
                                    ?>
                                    <div class="dietary-card">
                                        <div class="card-bg card-utilities">
                                            <div class="card-img-wrapper">
                                                <div class="cta-icon icon-only cta-icon-not-saved-white"  post_id = "<?php echo $dietary_requirement->term_id;?>" user_id  = "<?php echo $authorid;?>" >
                                                    <span>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/not-saved-white.svg" alt="Save recipe icon" />
                                                    </span>
                                                </div>

                                                <?php
                                                $image = get_field('image', $dietary_requirement);

                                                if(!empty($image)): ?>
                                                    <img src="<?php echo $image; ?>" />
                                                <?php endif; ?>

                                                <?php if (get_field('sponsor_name', $dietary_requirement)): ?>
                                                    <p class="recipe-sponsor">Sponsored by <?php the_field('sponsor_name', $dietary_requirement); ?></p>
                                                <?php endif; ?>
                                            </div>

                                            <div class="card-body">
                                                <h4 class="card-title heading-std-text"><?php echo $dietary_requirement->name; ?></h4>

                                                <?php $description_text = wp_trim_words($term_description, $num_words = 10, $more = '...'); ?>
                                                <p><?php echo $description_text; ?></p>
                                            </div>

                                            <div class="card-footer">
                                                <p><?php echo $dietary_requirement->count; ?> Recipes</p>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <div class="row no-gutters">
                    <!-- Advertisement -->
                    <div class="col-12">
                        <div class="advert-content content-block double-margin">
                            <div class="row justify-content-center no-gutters">
                                <div class="col">
                                    <!-- Async AdSlot 4 for Ad unit 'deliciousmagazine.co.uk/home' ### Size: [[970,250]] -->
                                    <!-- Adslot's refresh function: googletag.pubads().refresh([gptadslots[3]]) -->
                                    <div id='div-gpt-ad-1420540-4'>
                                      <script>
                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1420540-4'); });
                                      </script>
                                    </div>
                                    <!-- End AdSlot 4 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

<?php get_footer();
