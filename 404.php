<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <main class="main-content content">
        <div class="content-wrapper">
            <section>
                <div class="content-block">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="copy">
                                <h1>Oops!<br />404 Error Page</h1>
                                <p>We're sorry you can't find what you're looking for... why not take a look at our recipes for something else? Or, if all else fails, have a slice of cake!</p>
                                <a href="<?php echo home_url(); ?>" class="bttn">Go to homepage and explore</a>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="full-width">
                                <img src="http://deliciousnew.wpengine.com/wp-content/uploads/2018/08/pancakes.jpg" alt="Error image" class="img-responsive" />
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php get_template_part('template-parts/page/content', 'subscribe'); ?>
        </div>
    </main>

<?php get_footer();
